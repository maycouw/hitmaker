-------------------------------------------------------------------------------
-- Author      : Gjones
-------------------------------------------------------------------------------
-- Simple testbench for RD53B emulator hit generator sub-block (hitmaker.sv)
-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

entity  tb_hitmaker is
end     tb_hitmaker;

architecture struct of tb_hitmaker is

-- Clock freq expressed in MHz
constant CLK_FREQ_MHZ           : real      := 100.0;
-- Clock period
constant CLK_PER_MHZ            : time      := integer(1.0E+6/(CLK_FREQ_MHZ)) * 1 ps;

signal sim_done                 : boolean   := false;

signal clk                      : std_logic := '0';             
signal reset                    : std_logic := '1';           

signal start                    : std_logic := '0';     -- Begin stream generation
signal ready_out                : std_logic ;           -- Normally high. Goes low when any internal FIFO is over-threshold
signal busy                     : std_logic ;           -- Set low when din_dv=1 then high after last dout of an event   

-- Control parameters
signal reg_mode_evgen           : std_logic_vector( 1 downto  0) ;  -- Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
signal reg_seed                 : std_logic_vector(31 downto  0) ;  -- Seed to initialize the pseudo-random number generator
signal reg_hits_per_stream      : std_logic_vector(15 downto  0) ;  -- 
signal reg_lv_density           : std_logic_vector( 4 downto  0) ;  -- Parameter to set the probability of a vertical line of hits occurring. Can be zero
signal reg_lv_maxlen            : std_logic_vector( 4 downto  0) ;  -- Parameter to set the max length of a v-line of hits occurring
signal reg_cl_density           : std_logic_vector(11 downto  0) ;  -- Parameter to set the range of the increment in x,y from one cluster to the next.
signal reg_cl_max               : std_logic_vector(11 downto  0) ;  -- Parameter to set the max number of clusters+lines to generate.
signal reg_restart              : std_logic                      ;  -- 
signal reg_en_encode            : std_logic                      ;  -- '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
signal reg_en_tot               : std_logic                      ;  -- '1' = ToT enabled, '0'= No ToT data
signal reg_mode_tot             : std_logic                      ;  -- '0' = Constant (tot_value), '1' = Incrementing
signal reg_tot_value            : std_logic_vector( 3 downto  0) ;  -- ToT value for constant mode
signal reg_tag_init             : std_logic_vector( 7 downto  0) ;  -- Tag to start output stream. Subsequent tags (internal) will be 11-bit
signal reg_chip_id              : std_logic_vector( 1 downto  0) ;  --  
signal reg_en_chip_id           : std_logic                      ;  -- Enable chip ID[1:0]
signal reg_en_eos               : std_logic                      ;  -- Enable min 6 zeros output to mark end of stream

-- Output of raw event generator data, for comparison with resulting data after encoding then decoding 
signal hit_dv                   : std_logic                      ;             
signal hit_eos                  : std_logic                      ;              
signal hit_eoe                  : std_logic                      ;             
signal hit_qrow                 : std_logic_vector( 7 downto  0) ;    
signal hit_ccol                 : std_logic_vector( 5 downto  0) ;    
signal hit_hitmap               : std_logic_vector(15 downto  0) ;    

-- Output of encoded data
signal ready_in                 : std_logic;           
signal dout                     : std_logic_vector( 63 downto 0);   --  Output data. 
signal dout_eos                 : std_logic;                        --  Output data valid
signal dout_eoe                 : std_logic;                        --  Output data valid
signal dout_dv                  : std_logic;                        --  Output data valid
                         
---------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 1 to nclks loop
        wait until clk'event and clk='0';
    end loop;
end;

----------------------------------------------------------------
-- Print a string with no time or instance path.
----------------------------------------------------------------
procedure cpu_print_msg(
    constant msg    : in    string
) is
variable line_out   : line;
begin
    write(line_out, msg);
    writeline(output, line_out);
end procedure cpu_print_msg;


begin
    
    -------------------------------------------------------------
    -- Generate hit data and convert into 64-bit Aurora packets
    -------------------------------------------------------------
    u_hitmaker : entity work.hitmaker
    port map(
        clk                     => clk                      , -- input            
        reset                   => reset                    , -- input            

        start                   => start                    , -- input             Begin stream generation
        ready_out               => ready_out                , -- output            Normally high. Goes low when any internal FIFO is over-threshold
        busy                    => busy                     , -- output            Set low when din_dv=1 then high after last dout of an event   

        -- Control parameters
        reg_mode_evgen          => reg_mode_evgen           , -- input    [ 1:0]   Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
        reg_seed                => reg_seed                 , -- input    [31:0]   Seed to initialize the pseudo-random number generator
        reg_hits_per_stream     => reg_hits_per_stream      , -- input    [15:0]  
        reg_lv_density          => reg_lv_density           , -- input    [ 4:0]   Parameter to set the probability of a vertical line of hits occurring. Can be zero
        reg_lv_maxlen           => reg_lv_maxlen            , -- input    [ 4:0]   Parameter to set the max length of a v-line of hits occurring
        reg_cl_density          => reg_cl_density           , -- input    [11:0]   Parameter to set the range of the increment in x,y from one cluster to the next.
        reg_cl_max              => reg_cl_max               , -- input    [11:0]   Parameter to set the max number of clusters+lines to generate.
        reg_restart             => reg_restart              , -- input             
        reg_en_encode           => reg_en_encode            , -- input             '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
        reg_en_tot              => reg_en_tot               , -- input             '1' = ToT enabled, '0'= No ToT data
        reg_mode_tot            => reg_mode_tot             , -- input             '0' = Constant (tot_value), '1' = Incrementing
        reg_tot_value           => reg_tot_value            , -- input    [ 3:0]   ToT value for constant mode
        reg_tag_init            => reg_tag_init             , -- input    [ 7:0]   Tag to start output stream. Subsequent tags (internal) will be 11-bit
        reg_chip_id             => reg_chip_id              , -- input    [ 1:0]    
        reg_en_chip_id          => reg_en_chip_id           , -- input             Enable chip ID[1:0]
        reg_en_eos              => reg_en_eos               , -- input             Enable min 6 zeros output to mark end of stream

        -- Output of raw event generator data, for comparison with resulting data after encoding then decoding 
        hit_dv                  => hit_dv                   , -- output            
        hit_eos                 => hit_eos                  , -- output             
        hit_eoe                 => hit_eoe                  , -- output            
        hit_qrow                => hit_qrow                 , -- output   [ 7:0]   
        hit_ccol                => hit_ccol                 , -- output   [ 5:0]   
        hit_hitmap              => hit_hitmap               , -- output   [15:0]   

        -- Output of encoded data
        ready_in                => ready_in                 , -- input              Normally high from downstream sink. Goes low when sink cannot accept data
        dout                    => dout                     , -- output  [ 63:0]     
        dout_eos                => dout_eos                 , -- output             Set for end-of-stream. Last hit in stream          
        dout_eoe                => dout_eoe                 , -- output             Set for end-of-event. Last hit in event           
        dout_dv                 => dout_dv                    -- output             Set when new dout produced            
    );


    -------------------------------------------------------------
    -- Generate logic clock
    -------------------------------------------------------------
    pr_clk : process
    begin
        clk  <= '0';
        wait for (CLK_PER_MHZ/2);
        clk  <= '1';
        wait for (CLK_PER_MHZ-CLK_PER_MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;


    -------------------------------------------------------------
    -- Main 
    -------------------------------------------------------------
    pr_gen : process
    begin
        -- Reset
        reset                   <= '1';
        start                   <= '0';
        ready_in                <= '0';

        -- Register setup
        reg_mode_evgen          <= "00"         ; -- [ 1:0]   Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
        reg_seed                <= X"FFFFFFFF"  ; -- [31:0]   Seed to initialize the pseudo-random number generator
        reg_hits_per_stream     <= X"0004"      ; -- [15:0]  
        reg_lv_density          <= "00100"      ; -- [ 4:0]   Parameter to set the probability of a vertical line of hits occurring. Can be zero
        reg_lv_maxlen           <= "00100"      ; -- [ 4:0]   Parameter to set the max length of a v-line of hits occurring
        reg_cl_density          <= X"006"       ; -- [11:0]   Parameter to set the range of the increment in x,y from one cluster to the next.
        reg_cl_max              <= X"010"       ; -- [11:0]   Parameter to set the max number of clusters+lines to generate.
        reg_restart             <= '0'          ; --          
        reg_en_encode           <= '1'          ; --          '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
        reg_en_tot              <= '1'          ; --          '1' = ToT enabled, '0'= No ToT data
        reg_mode_tot            <= '0'          ; --          '0' = Constant (tot_value), '1' = Incrementing
        reg_tot_value           <= X"6"         ; -- [ 3:0]   ToT value for constant mode
        reg_tag_init            <= X"00"        ; -- [ 7:0]   Tag to start output stream. Subsequent tags (internal) will be 11-bit
        reg_chip_id             <= "01"         ; -- [ 1:0]    
        reg_en_chip_id          <= '1'          ; --          Enable chip ID[1:0]
        reg_en_eos              <= '1'          ; --          Enable min 6 zeros output to mark end of stream
        clk_delay(10);

        reset                   <= '0';
        clk_delay(10);

        ready_in                <= '1';
        clk_delay(5);

        -- Set 'start' high to begin hit generation
        start                   <= '1';
        clk_delay(1);
        start                   <= '0';
        clk_delay(5);

        -- Wait for 'busy' to indicated generation has begun then wait for it to end
        wait until busy = '1';
        wait until busy = '0';

        clk_delay(10);

        sim_done    <= true;    -- Set sim_done to stop clock generation and bring simulation to a halt
        wait;

    end process;


end struct;

