-------------------------------------------------------------------------------
-- Author      : Gjones
-------------------------------------------------------------------------------
-- Simple testbench for RD53B emulator hit generator sub-block hit_addr.sv
-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

--use     work.tb_rd53b_fpga_pkg.all;

entity tb_hit_addr is
end tb_hit_addr;

architecture struct of tb_hit_addr is

-- Clock freq expressed in MHz
constant CLK_FREQ_MHZ           : real      := 100.0;
-- Clock period
constant CLK_PER_MHZ            : time      := integer(1.0E+6/(CLK_FREQ_MHZ)) * 1 ps;

signal sim_done                 : boolean   := false;
signal busy                     : std_logic := '0';

signal clk                      : std_logic := '0';             
signal reset                    : std_logic := '1';           

-- input 
signal din_qrow                 : std_logic_vector( 7 downto 0);    -- 
signal din_ccol                 : std_logic_vector( 5 downto 0);    -- 
signal din_eos                  : std_logic; 
signal din_dv                   : std_logic; 

-- hit output
signal dout_dv                  : std_logic;                        -- Set when new dout_hit produced  
signal dout                     : std_logic_vector(15 downto 0);    -- Output data.  
signal dout_size                : std_logic_vector( 4 downto 0);    -- Number of bits in dout (2 to 16)
                         
---------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 1 to nclks loop
        wait until clk'event and clk='0';
    end loop;
end;

----------------------------------------------------------------
-- Print a string with no time or instance path.
----------------------------------------------------------------
procedure cpu_print_msg(
    constant msg    : in    string
) is
variable line_out   : line;
begin
    write(line_out, msg);
    writeline(output, line_out);
end procedure cpu_print_msg;

signal dbg_qrow     : std_logic_vector( 7 downto 0); 
signal dbg_ccol     : std_logic_vector( 5 downto 0); 
signal dbg_is_last  : std_logic; 
signal dbg_is_nbor  : std_logic; 

begin
    
    -------------------------------------------------------------
    -- 
    -------------------------------------------------------------
    u_hit_addr : entity work.hit_addr
    port map(
        clk         => clk          , -- input   logic           
        reset       => reset        , -- input   logic           

        din_qrow    => din_qrow     , -- input   logic   [ 7:0]     // 
        din_ccol    => din_ccol     , -- input   logic   [ 5:0]     // 
        din_eos     => din_eos      , -- input   logic              // Input data end-of-stream
        din_dv      => din_dv       , -- input   logic              // Input data valid

        dout        => dout         , -- output  logic   [15:0]     // Output data.  
        dout_size   => dout_size    , -- output  logic   [ 4:0]     // Number of bits in dout (2 to 16)
        dout_dv     => dout_dv        -- output  logic              // Output data valid
    );


    -------------------------------------------------------------
    -- display the output fields
    -------------------------------------------------------------
    pr_debug : process(dout, dout_size, dout_dv)
    begin
        
        if (dout_dv = '1') then

            if (dout_size = "10000") then
                dbg_ccol     <= dout(15 downto 10);
                dbg_is_nbor  <= dout(9); 
                dbg_is_last  <= dout(8);
                dbg_qrow     <= dout( 7 downto 0);
            
            elsif (dout_size = "01010") then
                dbg_ccol     <= "ZZZZZZ";
                dbg_is_nbor  <= dout(15); 
                dbg_is_last  <= dout(14);
                dbg_qrow     <= dout(13 downto 6);

            elsif (dout_size = "01000") then
                dbg_ccol     <= dout(15 downto 10);
                dbg_is_nbor  <= dout(9); 
                dbg_is_last  <= dout(8);
                dbg_qrow     <= "ZZZZZZZZ";

            elsif (dout_size = "00010") then
                dbg_ccol     <= "ZZZZZZ";
                dbg_is_nbor  <= dout(15); 
                dbg_is_last  <= dout(14);
                dbg_qrow     <= "ZZZZZZZZ";

            else
                dbg_ccol     <= "ZZZZZZ";
                dbg_is_nbor  <= 'Z'; 
                dbg_is_last  <= 'Z';
                dbg_qrow     <= "ZZZZZZZZ";

            end if; 
        else
            dbg_ccol     <= "ZZZZZZ";
            dbg_is_nbor  <= 'Z'; 
            dbg_is_last  <= 'Z';
            dbg_qrow     <= "ZZZZZZZZ";
        end if;            

    end process;



    -------------------------------------------------------------
    -- Generate logic clock
    -------------------------------------------------------------
    pr_clk : process
    begin
        clk  <= '0';
        wait for (CLK_PER_MHZ/2);
        clk  <= '1';
        wait for (CLK_PER_MHZ-CLK_PER_MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;


    -------------------------------------------------------------
    -- Main 
    -------------------------------------------------------------
    pr_gen : process
    begin
        -- Reset
        reset           <= '1';
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_eos         <= '0';
        din_dv          <= '0';

        clk_delay(10);
        reset           <= '0';
        clk_delay(10);

        -- Hit 0 First hit
        din_qrow        <= X"01";
        din_ccol        <= "000011";
        din_dv          <= '1';
        clk_delay(1);
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        -- Hit 1 Same col, NOT next row
        din_qrow        <= X"03";
        din_ccol        <= "000011";
        din_dv          <= '1';
        clk_delay(1);
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        -- Hit 2 Same col, next row
        din_qrow        <= X"04";
        din_ccol        <= "000011";
        din_dv          <= '1';
        clk_delay(1);
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(2);

        -- Hit 3 Same col, next row
        din_qrow        <= X"05";
        din_ccol        <= "000011";
        din_dv          <= '1';
        clk_delay(1);
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(4);

        -- Hit 4  New column, new row
        din_qrow        <= X"08";
        din_ccol        <= "001000";
        din_dv          <= '1';
        clk_delay(1);
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        -- Hit 5  New column, new row
        din_qrow        <= X"09";
        din_ccol        <= "000010";
        din_dv          <= '1';
        clk_delay(1);
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(8);

        -- Hit 6  New column, new row, end-of-stream
        din_qrow        <= X"0B";
        din_ccol        <= "001111";
        din_eos         <= '1';         -- EOS
        din_dv          <= '1';
        clk_delay(1);
        din_qrow        <= X"00";
        din_ccol        <= "000000";
        din_eos         <= '0';
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        wait for 1 us;

        sim_done    <= true;
        wait;

    end process;


end struct;

