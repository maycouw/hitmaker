`timescale 1ns/1ps

module tb_hit_barrelshift();
    logic clk, reset;

    logic [109:0] din;
    logic [7:0]   din_shift;
    logic         din_eos;
    logic         din_eoe;
    logic [7:0]   din_ptr;
    logic         din_dv;

    logic [251:0] dout;
    logic         dout_eos;
    logic         dout_eoe;
    logic [7:0]   dout_ptr;
    logic         dout_dv;

    logic         sim_done;
    
    hit_barrelshift dut
        (
         .clk,
         .reset,
        
         .din,
         .din_shift,
         .din_eos,
         .din_eoe,
         .din_ptr,
         .din_dv,

         .dout,
         .dout_eos,
         .dout_eoe,
         .dout_ptr,
         .dout_dv
         );
    
    parameter CLOCK_MHZ = 100;
    
    initial begin // clock generator
        sim_done = 0;
        clk <= 0;
        
        while(~sim_done) #(500/CLOCK_MHZ) clk <= ~clk;
    end

    initial begin // reset generator
        reset <= 1;
        repeat(10) @(posedge clk);      
        reset <= 0;
    end

    parameter COUNT = 252;
    parameter RANDOM_INPUT = 110'b000111_00101011_00101110_01100101_00010101_11001111_10111111_10101101_11101100_00111111_10100100_10010110_11000110_01110011;
    
    // inputs
    initial begin
        integer i;

        // random bitstring
        din <= RANDOM_INPUT;
        din_shift <= 0;
        din_eos <= 0;
        din_eoe <= 0;
        din_ptr <= 0;
        din_dv <= 0;

        // wait to come out of reset
        @(posedge clk);
        while(reset) @(posedge clk);

        for(i = 0; i <= COUNT; i++) begin
            din_shift <= i;
            din_dv <= 1;
            @(posedge clk);
            din_dv <= 0;
            @(posedge clk);
        end
        din_eos <= 1;
    end

    // output verification
    parameter EXTENDED_RESULT = {RANDOM_INPUT, 142'b0, RANDOM_INPUT, 142'b0};
    
    initial begin
        integer i;
        integer fails;

        // wait to come out of reset
        @(posedge clk);
        while(reset) @(posedge clk);

        fails = 0;

        for(i = 0; i <= COUNT; i++) begin
            while(~dout_dv) @(posedge clk);

            assert(dout == EXTENDED_RESULT[i +: 252]) else begin
                $display("FAILED (time %t):", $realtime);
                $display("shift:  %d", i);
                $display("input:  %b", RANDOM_INPUT);
                $display("output: %b", dout);
                $display("expect: %b", EXTENDED_RESULT[i +: 252]);
                
                $display("");

                fails++;
            end

            if(fails > 10) begin
                $error("Too many test failures; aborting");
                $stop;
            end
            
            while(dout_dv) @(posedge clk);
        end

        repeat(10) @(posedge clk);

        if(fails == 0) begin
            $display("No failures!");
        end
        
        sim_done <= 1;
    end
endmodule
