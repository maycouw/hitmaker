-------------------------------------------------------------------------------
-- Author      : Gjones
-------------------------------------------------------------------------------
-- Simple testbench for RD53B emulator hit generator sub-block hit_assembler.sv
-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

entity tb_hit_packer is
end tb_hit_packer;

architecture struct of tb_hit_packer is

-- Clock freq expressed in MHz
constant CLK_FREQ_MHZ           : real      := 100.0;
-- Clock period
constant CLK_PER_MHZ            : time      := integer(1.0E+6/(CLK_FREQ_MHZ)) * 1 ps;

signal sim_done                 : boolean   := false;
signal busy                     : std_logic := '0';

signal clk                      : std_logic := '0';             
signal reset                    : std_logic := '1';           

signal din_addr                 : std_logic_vector(15 downto 0);    --  Compressed address (in MSBs)
signal din_addr_size            : std_logic_vector( 4 downto 0);    --  Number of bits in compressed address
signal din_hitmap               : std_logic_vector(29 downto 0);    --  Encoded hitmap (in MSBs)
signal din_hitmap_size          : std_logic_vector( 4 downto 0);    --  Number of bits in encoded hitmap
signal din_tots                 : std_logic_vector(63 downto 0);    --  Up to 16 4-bit ToT values (in MSBs)
signal din_tots_size            : std_logic_vector( 6 downto 0);    --  Number of bits in ToT bus
signal din_eos                  : std_logic;                        --  Input data end-of-stream. Used to flush hits
signal din_eoe                  : std_logic;                        --  Input data end-of-event. 
signal din_dv                   : std_logic;                        --  Input data valid
signal dout                     : std_logic_vector(109 downto 0);   --  Output data. 
signal dout_size                : std_logic_vector(  6 downto 0);   --  Number of bits in endcoded hitmap
signal dout_eos                 : std_logic;                        --  Output data end-of-sequence
signal dout_dv                  : std_logic;                        --  Output data valid
                         
---------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 1 to nclks loop
        wait until clk'event and clk='0';
    end loop;
end;

----------------------------------------------------------------
-- Print a string with no time or instance path.
----------------------------------------------------------------
procedure cpu_print_msg(
    constant msg    : in    string
) is
variable line_out   : line;
begin
    write(line_out, msg);
    writeline(output, line_out);
end procedure cpu_print_msg;


begin
    
    -------------------------------------------------------------
    -- 
    -------------------------------------------------------------
    u_hit_packer : entity work.hit_packer
    port map(
        clk             => clk              ,   -- input   logic           
        reset           => reset            ,   -- input   logic           

        din_addr        => din_addr         ,   -- input   logic   [15:0]     Compressed address (in MSBs)
        din_addr_size   => din_addr_size    ,   -- input   logic   [ 4:0]     Number of bits in compressed address

        din_hitmap      => din_hitmap       ,   -- input   logic   [29:0]     Encoded hitmap (in MSBs)
        din_hitmap_size => din_hitmap_size  ,   -- input   logic   [ 4:0]     Number of bits in encoded hitmap

        din_tots        => din_tots         ,   -- input   logic   [63:0]     Up to 16 4-bit ToT values (in MSBs)
        din_tots_size   => din_tots_size    ,   -- input   logic   [ 6:0]     Number of bits in ToT bus

        din_eos         => din_eos          ,   -- input   logic              Input data end-of-stream. Used to flush hits
        din_eoe         => din_eoe          ,   -- input   logic              Input data end-of-event
        din_dv          => din_dv           ,   -- input   logic              Input data valid

        dout            => dout             ,   -- output  logic  [109:0]     Output data. 
        dout_size       => dout_size        ,   -- output  logic   [ 6:0]     Number of bits in endcoded hitmap
        dout_eos        => dout_eos         ,   -- output  logic              Output data end-of-sequence
        dout_dv         => dout_dv              -- output  logic              Output data valid
    );


    -------------------------------------------------------------
    -- Generate logic clock
    -------------------------------------------------------------
    pr_clk : process
    begin
        clk  <= '0';
        wait for (CLK_PER_MHZ/2);
        clk  <= '1';
        wait for (CLK_PER_MHZ-CLK_PER_MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;


    -------------------------------------------------------------
    -- Main 
    -------------------------------------------------------------
    pr_gen : process
    begin
        -- Reset
        reset           <= '1';
        din_addr        <= (others=>'0');  -- [15:0]  
        din_addr_size   <= (others=>'0');  -- [ 4:0]  
        din_hitmap      <= (others=>'0');  -- [29:0]  
        din_hitmap_size <= (others=>'0');  -- [ 4:0]  
        din_tots        <= (others=>'0');  -- [63:0]  
        din_tots_size   <= (others=>'0');  -- [ 6:0]  

        din_eos         <= '0';
        din_eoe         <= '0';
        din_dv          <= '0';

        clk_delay(10);
        reset           <= '0';
        clk_delay(10);

        ------------------------------------------------------- 
        -- Hit 0 : All fields at max size
        ------------------------------------------------------- 
        din_addr        <= "0101010101010101";
        din_addr_size   <= std_logic_vector(to_unsigned(16,5));
        din_hitmap      <= "001100110011001100110011001100";
        din_hitmap_size <= std_logic_vector(to_unsigned(30,5));
        din_tots        <= X"123456789ABCDEF0";
        din_tots_size   <= std_logic_vector(to_unsigned(64,7));
        din_eos         <= '1';
        din_dv          <= '1';
        clk_delay(1);
        din_addr        <= (others=>'0');  -- [15:0]  
        din_addr_size   <= (others=>'0');  -- [ 4:0]  
        din_hitmap      <= (others=>'0');  -- [29:0]  
        din_hitmap_size <= (others=>'0');  -- [ 4:0]  
        din_tots        <= (others=>'0');  -- [63:0]  
        din_tots_size   <= (others=>'0');  -- [ 6:0]  
        din_eos         <= '0';
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        ------------------------------------------------------- 
        -- Hit 1 : All fields at min size
        ------------------------------------------------------- 
        din_addr        <= "1100000000000000";
        din_addr_size   <= std_logic_vector(to_unsigned(2,5));
        din_hitmap      <= "000000000000000000000000000000";
        din_hitmap_size <= std_logic_vector(to_unsigned(4,5));
        din_tots        <= X"5XXXXXXXXXXXXXXX";
        din_tots_size   <= std_logic_vector(to_unsigned(4,7));
        din_eos         <= '1';
        din_dv          <= '1';
        clk_delay(1);
        din_addr        <= (others=>'0');  -- [15:0]  
        din_addr_size   <= (others=>'0');  -- [ 4:0]  
        din_hitmap      <= (others=>'0');  -- [29:0]  
        din_hitmap_size <= (others=>'0');  -- [ 4:0]  
        din_tots        <= (others=>'0');  -- [63:0]  
        din_tots_size   <= (others=>'0');  -- [ 6:0]  
        din_eos         <= '0';
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        ------------------------------------------------------- 
        -- Hit 2 : Addr-2, Hitmap 12, ToT 20 (5-hitmap bits set) 
        ------------------------------------------------------- 
        din_addr        <= "0100000000000000";
        din_addr_size   <= std_logic_vector(to_unsigned( 2,5));
        din_hitmap      <= "110111011101000000000000000000";
        din_hitmap_size <= std_logic_vector(to_unsigned(12,5));
        din_tots        <= X"54321XXXXXXXXXXX";
        din_tots_size   <= std_logic_vector(to_unsigned(20,7));
        din_eos         <= '1';
        din_dv          <= '1';
        clk_delay(1);
        din_addr        <= (others=>'0');  -- [15:0]  
        din_addr_size   <= (others=>'0');  -- [ 4:0]  
        din_hitmap      <= (others=>'0');  -- [29:0]  
        din_hitmap_size <= (others=>'0');  -- [ 4:0]  
        din_tots        <= (others=>'0');  -- [63:0]  
        din_tots_size   <= (others=>'0');  -- [ 6:0]  
        din_eos         <= '0';
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);


        --wait for 1 us;

        sim_done    <= true;
        wait;

    end process;


end struct;

