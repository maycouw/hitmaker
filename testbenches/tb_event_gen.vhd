-------------------------------------------------------------------------------
-- Author      : Gjones
-------------------------------------------------------------------------------
-- Simple testbench for event generator for event generator
-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

--use     work.tb_rd53b_fpga_pkg.all;

entity tb_event_gen is
end tb_event_gen;

architecture struct of tb_event_gen is

-- Clock freq expressed in MHz
constant CLK_FREQ_160MHZ        : real      := 160.0;
-- Clock period
constant CLK_PER_160MHZ         : time      := integer(1.0E+6/(CLK_FREQ_160MHZ)) * 1 ps;

signal clk160                   : std_logic := '0';
signal sim_done                 : boolean   := false;

signal clk                      : std_logic := '0';             
signal reset                    : std_logic := '1';           
signal ready                    : std_logic := '0';           
signal start                    : std_logic := '0';           
signal busy                     : std_logic;           

signal seed                     : std_logic_vector(31 downto 0) := X"FFFFFFFF"; -- 
-- Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
signal mode                     : std_logic_vector( 1 downto 0) := "00";        -- 

-- input options
signal hits_per_stream          : std_logic_vector(15 downto 0) := X"0000";     -- Set maximum number of hits in a stream 
signal lv_density               : std_logic_vector( 4 downto 0) := "00000";     -- Parameter to set the probability of a vertical line of hits occurring. Can be zero
signal lv_maxlen                : std_logic_vector( 4 downto 0) := "00000";     -- Parameter to set the max length of a v-line of hits occurring
signal cl_density               : std_logic_vector(11 downto 0) := X"000";      -- Parameter to set the range of the increment in x,y from one cluster to the next.
signal cl_max                   : std_logic_vector(11 downto 0) := X"000";      -- Parameter to set the max number of clusters+lines to generate.

-- hit output
signal dout_dv                  : std_logic;                        -- Set when new dout_hit produced  
signal dout_eos                 : std_logic;                        -- Set for end-of-stream. Last hit in stream
signal dout_hit_qrow            : std_logic_vector( 7 downto 0);    -- 
signal dout_hit_ccol            : std_logic_vector( 5 downto 0);    -- 
signal dout_hit_hitmap          : std_logic_vector(15 downto 0);    -- 

-------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 1 to nclks loop
        wait until clk'event and clk='0';
    end loop;
end;

----------------------------------------------------------------
-- Print a string with no time or instance path.
----------------------------------------------------------------
procedure cpu_print_msg(
    constant msg    : in    string
) is
variable line_out   : line;
begin
    write(line_out, msg);
    writeline(output, line_out);
end procedure cpu_print_msg;


begin
    
    -------------------------------------------------------------
    -- 
    -------------------------------------------------------------
    u_event_gen : entity work.event_gen_incr
    port map(
        clk             => clk               , -- input   logic           
        reset           => reset             , -- input   logic           
        start           => start             , -- input   logic           Begin stream generation
        mode            => mode              , -- input   logic   [ 1:0]  Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
        ready           => ready             , -- input   logic           Downstream block can accept new hit data
        busy            => busy              , -- output  logic           Set low when din_dv=1 then high before last dout   
        seed            => seed              , -- input   logic   [31:0]  Seed to initialize the pseudo-random number generator
        hits_per_stream => hits_per_stream   , -- input   logic   [15:0]  Max number of hits in a stream
        lv_density      => lv_density        , -- input   logic   [ 4:0]  Parameter to set the probability of a vertical line of hits occurring. Can be zero
        lv_maxlen       => lv_maxlen         , -- input   logic   [ 4:0]  Parameter to set the max length of a v-line of hits occurring
        cl_density      => cl_density        , -- input   logic   [11:0]  Parameter to set the range of the increment in x,y from one cluster to the next.
        cl_max          => cl_max            , -- input   logic   [11:0]  Parameter to set the max number of clusters+lines to generate.
        dout_dv         => dout_dv           , -- output  logic           Set when new dout_hit produced  
        dout_eos        => dout_eos          , -- output  logic           Set for end-of-stream. Last hit in stream 
        dout_hit_qrow   => dout_hit_qrow     , -- output  logic   [ 7:0]  
        dout_hit_ccol   => dout_hit_ccol     , -- output  logic   [ 5:0]  
        dout_hit_hitmap => dout_hit_hitmap     -- output  logic   [15:0]  
    );



    -------------------------------------------------------------
    -- Generate logic clock
    -------------------------------------------------------------
    pr_clk : process
    begin
        clk  <= '0';
        wait for (CLK_PER_160MHZ/2);
        clk  <= '1';
        wait for (CLK_PER_160MHZ-CLK_PER_160MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;


    -------------------------------------------------------------
    -- Main 
    -------------------------------------------------------------
    pr_gen : process
    begin
        -- Reset
        reset           <= '1';
        ready           <= '1';
        mode            <= "00";
        seed            <= X"55555555"; -- 
        start           <= '0';

        clk_delay(10);
        reset           <= '0';
        clk_delay(10);

        hits_per_stream <= X"0020";     -- Set maximum number of hits in a stream 
        start           <= '1';
        clk_delay(1);
        start           <= '0';

        wait until busy = '0';
        wait for 1 us;

        sim_done    <= true;
        wait;

    end process;


end struct;

