-------------------------------------------------------------------------------
-- hit_decoder for testing hit_checker. 
--
-- Outputs a preset pattern of decoded hit data
-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

entity hit_decoder is
port (
    clk             : in  std_logic                     ;   -- 
    reset           : in  std_logic                     ;   -- 
    ready           : out std_logic                     ;   -- When the engine is ready to recieve raw stream data
    status          : out std_logic_vector( 7 downto 0) ;   -- TBD status bits [7:0]

    -- Control settings (may, or may not be implemented)
    reg_en_encode   : in  std_logic                     ;   -- input  '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
    reg_en_tot      : in  std_logic                     ;   -- input  '1' = ToT enabled, '0'= No ToT data
    reg_en_chip_id  : in  std_logic                     ;   -- input  Enable chip ID[1:0]
    reg_en_eos      : in  std_logic                     ;   -- input  Enable min 6 zeros output to mark end of stream

    din             : in  std_logic_vector(63 downto 0) ;   -- Raw data from the data stream
    din_eoe         : in  std_logic                     ;   -- Tells engine when the current event ends. Pass to dout_eoe
    din_dv          : in  std_logic                     ;   -- The stream data that is being advertised is valid

    dout_chip_id    : out std_logic_vector( 1 downto 0) ;   -- output current chip_id [1:0]            
    dout_tag        : out std_logic_vector( 7 downto 0) ;   -- output current tag     [7:0]  
    dout_eos        : out std_logic                     ;   -- output end-of-stream            
    dout_eoe        : out std_logic                     ;   -- output end-of-event           
    dout_qrow       : out std_logic_vector( 7 downto 0) ;   -- output qrow    [ 7:0]   
    dout_ccol       : out std_logic_vector( 5 downto 0) ;   -- output ccol    [ 5:0]   
    dout_hitmap     : out std_logic_vector(15 downto 0) ;   -- output hitmap  [15:0]   
    dout_tot        : out std_logic_vector(63 downto 0) ;   -- output tot     [63:0] ToT data packed into MSBs of bus
    dout_dv         : out std_logic                         -- output data valid           
);
end     hit_decoder;
    

architecture test of hit_decoder is
---------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 1 to nclks loop
        wait until clk'event and clk='0';
    end loop;
end;

begin

    -------------------------------------------------------------
    -- Main 
    -------------------------------------------------------------
    pr_gen : process
    begin

        dout_chip_id        <= "00"     ; --  output current chip_id [1:0]            
        dout_tag            <= X"00"    ; --  output current tag     [7:0]  
        dout_eos            <= '0'      ; --  output end-of-stream            
        dout_eoe            <= '0'      ; --  output end-of-event           
        dout_qrow           <= X"00"    ; --  output qrow    [ 7:0]   
        dout_ccol           <= "000000" ; --  output ccol    [ 5:0]   
        dout_hitmap         <= X"0000"  ; --  output hitmap  [15:0]   
        dout_tot            <= X"0000000000000000"  ; --  output tot     [63:0] ToT data packed into MSBs of bus
        dout_dv             <= '0'      ; --  output data valid           

        -- Reset
        wait until reset = '0';
        clk_delay(10);
        ready               <= '1';
        clk_delay(2);
        wait until din_dv = '1';
        clk_delay(2);

        ---------------------------------------------------------
        -- 0001
        ---------------------------------------------------------
        dout_chip_id        <= "01"     ; --  output current chip_id [1:0]            
        dout_tag            <= X"05"    ; --  output current tag     [7:0]  
        dout_eos            <= '0'      ; --  output end-of-stream            
        dout_eoe            <= '0'      ; --  output end-of-event           
        dout_qrow           <= X"01"    ; --  output qrow    [ 7:0]   
        dout_ccol           <= "000001" ; --  output ccol    [ 5:0]   
        dout_hitmap         <= X"0001"  ; --  output hitmap  [15:0]   
        dout_tot            <= X"3000000000000000"  ; --  output tot     [63:0] ToT data packed into MSBs of bus
        dout_dv             <= '1'      ; --  output data valid           
        clk_delay(1);
        dout_qrow           <= X"00"    ; --  output qrow    [ 7:0]   
        dout_ccol           <= "000000" ; --  output ccol    [ 5:0]   
        dout_hitmap         <= X"0000"  ; --  output hitmap  [15:0]   
        dout_tot            <= X"0000000000000000"  ; --  output tot     [63:0] ToT data packed into MSBs of bus
        dout_dv             <= '0'      ; --  output data valid           
        clk_delay(1);

        ---------------------------------------------------------
        -- 0x0002
        ---------------------------------------------------------
        dout_chip_id        <= "01"     ;             
        dout_tag            <= X"05"    ;   
        dout_eos            <= '0'      ;     
        dout_eoe            <= '0'      ;   
        dout_qrow           <= X"03"    ; 
        dout_ccol           <= "000001" ; 
        dout_hitmap         <= X"0002"  ; 
        dout_tot            <= X"3000000000000000";
        dout_dv             <= '1'      ;        
        clk_delay(1);
        dout_qrow           <= X"00"    ;    
        dout_ccol           <= "000000" ;    
        dout_hitmap         <= X"0000"  ;    
        dout_tot            <= X"0000000000000000"; 
        dout_dv             <= '0'      ; 
        clk_delay(1);

        ---------------------------------------------------------
        -- 0x0003
        ---------------------------------------------------------
        dout_chip_id        <= "01"     ;             
        dout_tag            <= X"05"    ;   
        dout_eos            <= '0'      ;     
        dout_eoe            <= '0'      ;   
        dout_qrow           <= X"05"    ; 
        dout_ccol           <= "000001" ; 
        dout_hitmap         <= X"0003"  ; 
        dout_tot            <= X"3300000000000000";
        dout_dv             <= '1'      ;        
        clk_delay(1);
        dout_qrow           <= X"00"    ;    
        dout_ccol           <= "000000" ;    
        dout_hitmap         <= X"0000"  ;    
        dout_tot            <= X"0000000000000000"; 
        dout_dv             <= '0'      ; 
        clk_delay(1);

        ---------------------------------------------------------
        -- 0x0004
        ---------------------------------------------------------
        dout_chip_id        <= "01"     ;             
        dout_tag            <= X"05"    ;   
        dout_eos            <= '0'      ;     
        dout_eoe            <= '0'      ;   
        dout_qrow           <= X"07"    ; 
        dout_ccol           <= "000001" ; 
        dout_hitmap         <= X"0004"  ; 
        dout_tot            <= X"3000000000000000";
        dout_dv             <= '1'      ;        
        clk_delay(1);
        dout_qrow           <= X"00"    ;    
        dout_ccol           <= "000000" ;    
        dout_hitmap         <= X"0000"  ;    
        dout_tot            <= X"0000000000000000"; 
        dout_dv             <= '0'      ; 
        clk_delay(1);

        ---------------------------------------------------------
        -- 0x0005
        ---------------------------------------------------------
        dout_chip_id        <= "01"     ;             
        dout_tag            <= X"05"    ;   
        dout_eos            <= '0'      ;     
        dout_eoe            <= '0'      ;   
        dout_qrow           <= X"09"    ; 
        dout_ccol           <= "000001" ; 
        dout_hitmap         <= X"0005"  ; 
        dout_tot            <= X"3300000000000000";
        dout_dv             <= '1'      ;        
        clk_delay(1);
        dout_qrow           <= X"00"    ;    
        dout_ccol           <= "000000" ;    
        dout_hitmap         <= X"0000"  ;    
        dout_tot            <= X"0000000000000000"; 
        dout_dv             <= '0'      ; 
        clk_delay(5);


        wait;

    end process;



end test;

