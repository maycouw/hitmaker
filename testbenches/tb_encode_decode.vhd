-------------------------------------------------------------------------------
-- Author      : Gjones
-------------------------------------------------------------------------------
-- Testbench for testing RD53B output Aurora bus decoding.
-- Uses hitmaker block to generate patterns and compressed data.
-- Testbench compares decoded data against original data.
-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

entity  tb_encode_decode is
end     tb_encode_decode;

architecture struct of tb_encode_decode is

-- Clock freq expressed in MHz
constant CLK_FREQ_MHZ           : real      := 100.0;
-- Clock period
constant CLK_PER_MHZ            : time      := integer(1.0E+6/(CLK_FREQ_MHZ)) * 1 ps;

signal sim_done                 : boolean   := false;

signal clk                      : std_logic := '0';             
signal reset                    : std_logic := '1';           

signal hitmaker_start           : std_logic ;                       -- Begin stream generation
signal hitmaker_ready           : std_logic ;                       -- Normally high. Goes low when any internal FIFO is over-threshold
signal hitmaker_busy            : std_logic ;                       -- Set low when din_dv=1 then high after last dout of an event   

-- Control parameters
signal reg_mode_evgen           : std_logic_vector( 1 downto  0) ;  -- Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
signal reg_seed                 : std_logic_vector(31 downto  0) ;  -- Seed to initialize the pseudo-random number generator
signal reg_hits_per_stream      : std_logic_vector(15 downto  0) ;  -- 
signal reg_lv_density           : std_logic_vector( 4 downto  0) ;  -- Parameter to set the probability of a vertical line of hits occurring. Can be zero
signal reg_lv_maxlen            : std_logic_vector( 4 downto  0) ;  -- Parameter to set the max length of a v-line of hits occurring
signal reg_cl_density           : std_logic_vector(11 downto  0) ;  -- Parameter to set the range of the increment in x,y from one cluster to the next.
signal reg_cl_max               : std_logic_vector(11 downto  0) ;  -- Parameter to set the max number of clusters+lines to generate.
signal reg_restart              : std_logic                      ;  -- 
signal reg_en_encode            : std_logic                      ;  -- '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
signal reg_en_tot               : std_logic                      ;  -- '1' = ToT enabled, '0'= No ToT data
signal reg_mode_tot             : std_logic                      ;  -- '0' = Constant (tot_value), '1' = Incrementing
signal reg_tot_value            : std_logic_vector( 3 downto  0) ;  -- ToT value for constant mode
signal reg_tag_init             : std_logic_vector( 7 downto  0) ;  -- Tag to start output stream. Subsequent tags (internal) will be 11-bit
signal reg_chip_id              : std_logic_vector( 1 downto  0) ;  --  
signal reg_en_chip_id           : std_logic                      ;  -- Enable chip ID[1:0]
signal reg_en_eos               : std_logic                      ;  -- Enable min 6 zeros output to mark end of stream

-- Hitmaker output of raw event generator data, for comparison with resulting data after encoding then decoding 
-- 'Expected' data
signal exp_hit_eos              : std_logic                      ;              
signal exp_hit_eoe              : std_logic                      ;             
signal exp_hit_qrow             : std_logic_vector( 7 downto  0) ;    
signal exp_hit_ccol             : std_logic_vector( 5 downto  0) ;    
signal exp_hit_hitmap           : std_logic_vector(15 downto  0) ;    
signal exp_hit_dv               : std_logic                      ;             
signal exp_hit_tot              : std_logic_vector(63 downto  0) ; -- hitmaker ToT data
signal exp_hit_tot_dv           : std_logic                      ; -- expected tot data valid

-- Output of encoded data
signal hitmaker_dout            : std_logic_vector( 63 downto 0);   --  Output data. 
signal hitmaker_dout_eos        : std_logic;                        --  
signal hitmaker_dout_eoe        : std_logic;                        --  
signal hitmaker_dout_dv         : std_logic;                        --  Output data valid
                         
-- Decoder output of decoded hit data fields
signal decoder_chip_id          : std_logic_vector( 1 downto  0) ;   
signal decoder_tag              : std_logic_vector( 7 downto  0) ;   
signal decoder_eos              : std_logic                      ;              
signal decoder_eoe              : std_logic                      ;         
signal decoder_qrow             : std_logic_vector( 7 downto  0) ;   
signal decoder_ccol             : std_logic_vector( 5 downto  0) ;   
signal decoder_hitmap           : std_logic_vector(15 downto  0) ;   
signal decoder_tot              : std_logic_vector(63 downto  0) ;  -- ToT data packed into MSBs of bus. Lower bits zero
signal decoder_dv               : std_logic                      ;       

signal decoder_ready            : std_logic;           
signal decoder_status           : std_logic_vector( 7 downto 0)  ;           

-- Checker 
signal checker_status           : std_logic_vector( 7 downto 0)  ;           
signal checker_err_dv           : std_logic;           
signal checker_errs             : std_logic_vector( 7 downto 0)  ;           
signal checker_err_count        : std_logic_vector( 7 downto 0)  ;           

---------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 1 to nclks loop
        wait until clk'event and clk='0';
    end loop;
end;

----------------------------------------------------------------
-- Print a string with no time or instance path.
----------------------------------------------------------------
procedure cpu_print_msg(
    constant msg    : in    string
) is
variable line_out   : line;
begin
    write(line_out, msg);
    writeline(output, line_out);
end procedure cpu_print_msg;


begin
    
    -------------------------------------------------------------
    -- Generate hit data and convert into 64-bit Aurora packets
    -------------------------------------------------------------
    u_hitmaker : entity work.hitmaker
    port map(
        clk                     => clk                      , -- input            
        reset                   => reset                    , -- input            

        start                   => hitmaker_start           , -- input             Begin stream generation
        ready_out               => hitmaker_ready           , -- output            Normally high. Goes low when any internal FIFO is over-threshold
        busy                    => hitmaker_busy            , -- output            Set low when din_dv=1 then high after last dout of an event   

        -- Control parameters
        reg_mode_evgen          => reg_mode_evgen           , -- input    [ 1:0]   Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
        reg_seed                => reg_seed                 , -- input    [31:0]   Seed to initialize the pseudo-random number generator
        reg_hits_per_stream     => reg_hits_per_stream      , -- input    [15:0]  
        reg_lv_density          => reg_lv_density           , -- input    [ 4:0]   Parameter to set the probability of a vertical line of hits occurring. Can be zero
        reg_lv_maxlen           => reg_lv_maxlen            , -- input    [ 4:0]   Parameter to set the max length of a v-line of hits occurring
        reg_cl_density          => reg_cl_density           , -- input    [11:0]   Parameter to set the range of the increment in x,y from one cluster to the next.
        reg_cl_max              => reg_cl_max               , -- input    [11:0]   Parameter to set the max number of clusters+lines to generate.
        reg_restart             => reg_restart              , -- input             

        reg_en_encode           => reg_en_encode            , -- input             '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
        reg_en_tot              => reg_en_tot               , -- input             '1' = ToT enabled, '0'= No ToT data
        reg_mode_tot            => reg_mode_tot             , -- input             '0' = Constant (tot_value), '1' = Incrementing
        reg_tot_value           => reg_tot_value            , -- input    [ 3:0]   ToT value for constant mode
        reg_tag_init            => reg_tag_init             , -- input    [ 7:0]   Tag to start output stream. Subsequent tags (internal) will be 11-bit
        reg_chip_id             => reg_chip_id              , -- input    [ 1:0]    
        reg_en_chip_id          => reg_en_chip_id           , -- input             Enable chip ID[1:0]
        reg_en_eos              => reg_en_eos               , -- input             Enable min 6 zeros output to mark end of stream

        -- Output of raw event generator data, for comparison with resulting data after encoding then decoding 
        hit_dv                  => exp_hit_dv               , -- output   expected data valid
        hit_eos                 => exp_hit_eos              , -- output             
        hit_eoe                 => exp_hit_eoe              , -- output            
        hit_qrow                => exp_hit_qrow             , -- output   [ 7:0]   
        hit_ccol                => exp_hit_ccol             , -- output   [ 5:0]   
        hit_hitmap              => exp_hit_hitmap           , -- output   [15:0]   

        hit_tot                 => exp_hit_tot              , -- output   [64:0]   
        hit_tot_dv              => exp_hit_tot_dv           , -- output   expected tot data valid

        -- Output of encoded data
        ready_in                => decoder_ready            , -- input              Normally high from downstream sink. Goes low when sink cannot accept data
        dout                    => hitmaker_dout            , -- output  [ 63:0]     
        dout_eos                => hitmaker_dout_eos        , -- output             Set for end-of-stream. Last hit in stream          
        dout_eoe                => hitmaker_dout_eoe        , -- output             Set for end-of-event. Last hit in event           
        dout_dv                 => hitmaker_dout_dv           -- output             Set when new dout produced            
    );


    -------------------------------------------------------------
    -- Hit decoder to convert 64-bit 'Aurora-formatted' words 
    -- back into uncompressed, separate fields.
    -------------------------------------------------------------
    u_hit_decoder : entity work.hit_decoder
    port map (
        clk                     => clk                      , -- 
        reset                   => reset                    , -- 

        ready                   => decoder_ready            , -- When the engine is ready to recieve raw stream data
        status                  => decoder_status           , -- TBD status bits [7:0]

        -- Control settings (may, or may not be implemented)
        reg_en_encode           => reg_en_encode            , -- input  '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
        reg_en_tot              => reg_en_tot               , -- input  '1' = ToT enabled, '0'= No ToT data
        reg_en_chip_id          => reg_en_chip_id           , -- input  Enable chip ID[1:0]
        reg_en_eos              => reg_en_eos               , -- input  Enable min 6 zeros output to mark end of stream

        din                     => hitmaker_dout            , -- Raw data from the data stream
        din_eoe                 => hitmaker_dout_eoe        , -- Tells engine when the current event ends. Pass to dout_eoe
        din_dv                  => hitmaker_dout_dv         , -- The stream data that is being advertised is valid

        -- Decoded hit data 
        dout_chip_id            => decoder_chip_id          , -- output current chip_id [1:0]            
        dout_tag                => decoder_tag              , -- output current tag     [7:0]  
        dout_eos                => decoder_eos              , -- output end-of-stream            
        dout_eoe                => decoder_eoe              , -- output end-of-event           
        dout_qrow               => decoder_qrow             , -- output qrow    [ 7:0]   
        dout_ccol               => decoder_ccol             , -- output ccol    [ 5:0]   
        dout_hitmap             => decoder_hitmap           , -- output hitmap  [15:0]   
        dout_tot                => decoder_tot              , -- output tot     [63:0] ToT data packed into MSBs of bus
        dout_dv                 => decoder_dv                 -- output data valid           
    );


    -------------------------------------------------------------
    -- hit_checker. 
    -- Stores generated hit data in a FIFO. Reads it out and 
    -- compares expected value agains actual decoded value.
    -- Expected ToT is made from tot mode register controls and 
    -- expected hitmap size.
    -------------------------------------------------------------
    -- checker_fail bits
    -------------------------------------------------------------
    -- Bit    Failure type
    --  0        chip_id
    --  1        tag
    --  2        qrow
    --  3        ccol
    --  4        hitmap
    --  5        ToT
    --  6        EOS
    --  7        EOE
    -------------------------------------------------------------
    u_hit_checker : entity work.hit_checker
    port map (
        clk                     => clk                      ,  
        reset                   => reset                    ,  
        restart                 => reg_restart              ,  

        -- Register setup. Operating mode
        reg_en_tot              => reg_en_tot               , -- input             '1' = ToT enabled, '0'= No ToT data
        reg_mode_tot            => reg_mode_tot             , -- input             '0' = Constant (tot_value), '1' = Incrementing
        reg_tot_value           => reg_tot_value            , -- input    [ 3:0]   ToT value for constant mode
        reg_tag_init            => reg_tag_init             , -- input    [ 7:0]   Tag to start output stream. Subsequent tags (internal) will be 11-bit
        reg_chip_id             => reg_chip_id              , -- input    [ 1:0]    
        reg_en_chip_id          => reg_en_chip_id           , -- input             Enable chip ID[1:0]
        reg_en_eos              => reg_en_eos               , -- input             Enable min 6 zeros output to mark end of stream

        status                  => checker_status           , -- TBD status bits [7:0]

        -- Expected data from hitmaker
        exp_eos                 => exp_hit_eos              , -- input             
        exp_eoe                 => exp_hit_eoe              , -- input            
        exp_qrow                => exp_hit_qrow             , -- input   [ 7:0]   
        exp_ccol                => exp_hit_ccol             , -- input   [ 5:0]   
        exp_hitmap              => exp_hit_hitmap           , -- input   [15:0]   
        exp_dv                  => exp_hit_dv               , -- input   expected data valid

        exp_tot                 => exp_hit_tot              , -- input   [15:0]   
        exp_tot_dv              => exp_hit_tot_dv           , -- input   expected data valid

        -- Actual data from decoder 
        act_chip_id             => decoder_chip_id          , -- input current chip_id [ 1:0]           
        act_tag                 => decoder_tag              , -- input current tag     [ 7:0]      
        act_eos                 => decoder_eos              , -- input end-of-stream            
        act_eoe                 => decoder_eoe              , -- input end-of-event    Used to check incrementing tag
        act_qrow                => decoder_qrow             , -- input qrow    [ 7:0]   
        act_ccol                => decoder_ccol             , -- input ccol    [ 5:0]   
        act_hitmap              => decoder_hitmap           , -- input hitmap  [15:0]   
        act_tot                 => decoder_tot              , -- input tot     [63:0] ToT data packed into MSBs of bus
        act_dv                  => decoder_dv               , -- input data valid           

        dout_err_dv             => checker_err_dv           , -- output result valid
        dout_errs               => checker_errs             , -- output fail bus [7:0]
        dout_err_count          => checker_err_count          -- output result error count [7:0]
    );


    -------------------------------------------------------------
    -- Generate logic clock
    -------------------------------------------------------------
    pr_clk : process
    begin
        clk  <= '0';
        wait for (CLK_PER_MHZ/2);
        clk  <= '1';
        wait for (CLK_PER_MHZ-CLK_PER_MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;


    -------------------------------------------------------------
    -- Main 
    -------------------------------------------------------------
    pr_gen : process
    begin
        -- Reset
        reset                   <= '1';
        hitmaker_start          <= '0';

        -- Register setup
        reg_mode_evgen          <= "00"         ; -- [ 1:0]   Type of data 00 = incrementing hitmap, 01 = 3x3 clusters, 10 cluster + vertical line, 11 cluster+vert+horiz lines
        reg_seed                <= X"FFFFFFFF"  ; -- [31:0]   Seed to initialize the pseudo-random number generator
        reg_hits_per_stream     <= X"FFFF"      ; -- [15:0]  
        reg_lv_density          <= "00100"      ; -- [ 4:0]   Parameter to set the probability of a vertical line of hits occurring. Can be zero
        reg_lv_maxlen           <= "00100"      ; -- [ 4:0]   Parameter to set the max length of a v-line of hits occurring
        reg_cl_density          <= X"006"       ; -- [11:0]   Parameter to set the range of the increment in x,y from one cluster to the next.
        reg_cl_max              <= X"010"       ; -- [11:0]   Parameter to set the max number of clusters+lines to generate.
        reg_restart             <= '0'          ; --          
        reg_en_encode           <= '1'          ; --          '1' = Encode (Compress) din. '0' = dout is din, dout_size = 16 (5'b1000)
        reg_en_tot              <= '1'          ; --          '1' = ToT enabled, '0'= No ToT data
        reg_mode_tot            <= '0'          ; --          '0' = Constant (tot_value), '1' = Incrementing
        reg_tot_value           <= X"3"         ; -- [ 3:0]   ToT value for constant mode
        reg_tag_init            <= X"05"        ; -- [ 7:0]   Tag to start output stream. Subsequent tags (internal) will be 11-bit
        reg_chip_id             <= "01"         ; -- [ 1:0]    
        reg_en_chip_id          <= '1'          ; --          Enable chip ID[1:0]
        reg_en_eos              <= '1'          ; --          Enable min 6 zeros output to mark end of stream
        clk_delay(10);

        reset                   <= '0';
        clk_delay(10);
        clk_delay(5);

        -- Set 'start' high to begin hit generation
        hitmaker_start          <= '1';
        clk_delay(1);
        hitmaker_start          <= '0';
        clk_delay(5);

        -- Wait for 'busy' to indicated generation has begun then wait for it to end
        wait until hitmaker_busy = '1';
        wait until hitmaker_busy = '0';
        
        wait until decoder_status(0) = '0'; -- indicating decoder is not busy
        clk_delay(10);                                                        
        

        sim_done    <= true;    -- Set sim_done to stop clock generation and bring simulation to a halt
        wait;

    end process;


end struct;

