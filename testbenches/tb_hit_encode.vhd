-------------------------------------------------------------------------------
-- Author      : Gjones
-------------------------------------------------------------------------------
-- Simple testbench for RD53B emulator hit generator sub-block hit_encode.sv
-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

entity tb_hit_encode is
end tb_hit_encode;

architecture struct of tb_hit_encode is

-- Clock freq expressed in MHz
constant CLK_FREQ_MHZ           : real      := 100.0;
-- Clock period
constant CLK_PER_MHZ            : time      := integer(1.0E+6/(CLK_FREQ_MHZ)) * 1 ps;

signal sim_done                 : boolean   := false;
signal busy                     : std_logic := '0';

signal clk                      : std_logic := '0';             
signal reset                    : std_logic := '1';           

-- Controls
signal en_encode                : std_logic := '1';           

-- input 
signal din                      : std_logic_vector(15 downto 0);    -- 
signal din_eos                  : std_logic; 
signal din_dv                   : std_logic; 

-- ToT output
signal dout                     : std_logic_vector(29 downto 0);    -- Output data.  
signal dout_size                : std_logic_vector( 4 downto 0);    -- Number of bits in dout (2 to 16)
signal dout_dv                  : std_logic;                        -- Set when new dout_hit produced  
                         
---------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 1 to nclks loop
        wait until clk'event and clk='0';
    end loop;
end;

----------------------------------------------------------------
-- Print a string with no time or instance path.
----------------------------------------------------------------
procedure cpu_print_msg(
    constant msg    : in    string
) is
variable line_out   : line;
begin
    write(line_out, msg);
    writeline(output, line_out);
end procedure cpu_print_msg;


begin
    
    -------------------------------------------------------------
    -- 
    -------------------------------------------------------------
    u_hit_encode : entity work.hit_encode
    port map(
        clk         => clk          , -- input   logic           
        reset       => reset        , -- input   logic           

        en_encode   => en_encode    , -- input   logic             // '1' = ToT enabled, '0'= No ToT data

        din         => din          , -- input   logic   [15:0]     // 
        din_eos     => din_eos      , -- input   logic              // Input data end-of-stream
        din_dv      => din_dv       , -- input   logic              // Input data valid

        dout        => dout         , -- output  logic   [29:0]     // Output data.  
        dout_size   => dout_size    , -- output  logic   [ 4:0]     // Number of bits in dout (2 to 16)
        dout_dv     => dout_dv        -- output  logic              // Output data valid
    );



    -------------------------------------------------------------
    -- Generate logic clock
    -------------------------------------------------------------
    pr_clk : process
    begin
        clk  <= '0';
        wait for (CLK_PER_MHZ/2);
        clk  <= '1';
        wait for (CLK_PER_MHZ-CLK_PER_MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;


    -------------------------------------------------------------
    -- Main 
    -------------------------------------------------------------
    pr_gen : process
    begin
        -- Reset
        reset           <= '1';
        en_encode       <= '1';           
        din             <= X"0000";
        din_eos         <= '0';
        din_dv          <= '0';

        clk_delay(10);
        reset           <= '0';
        clk_delay(10);

        -- Hit 0
        din             <= X"0001";
        din_dv          <= '1';
        clk_delay(1);
        din             <= X"0000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        -- Hit 1
        din             <= X"0003";
        din_dv          <= '1';
        clk_delay(1);
        din             <= X"0000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        -- Hit 2
        din             <= X"0007";
        din_dv          <= '1';
        clk_delay(1);
        din             <= X"0000";
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        -- Hit 3
        din             <= X"FFFF";
        din_eos         <= '1';
        din_dv          <= '1';
        clk_delay(1);
        din             <= X"0000";
        din_eos         <= '0';
        din_dv          <= '0';
        clk_delay(1);

        clk_delay(5);

        wait for 1 us;

        sim_done    <= true;
        wait;

    end process;


end struct;

