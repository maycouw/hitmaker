`timescale 1ns/1ps

module tb_hit_assembler();
    logic         clk, reset, restart;

    logic [7:0]   tag_init;
    logic [1:0]   chip_id;
    logic         en_chip_id;
    logic         en_eos;
    
    logic [109:0] din;
    logic [6:0]   din_size;
    logic         din_eos;
    logic         din_eoe;
    logic         din_dv;
    logic         ready_out;

    logic         ready_in;
    logic [63:0]  dout;
    logic         dout_eos;
    logic         dout_eoe;
    logic         dout_dv;

    logic         sim_done;
    
    hit_assembler dut
        (
         .clk, .reset, .restart,
         .tag_init, .chip_id, .en_chip_id, .en_eos,

         .din, .din_size, .din_eos, .din_eoe, .din_dv, .ready_out,
         .ready_in, .dout, .dout_eos, .dout_eoe, .dout_dv
         );

    parameter CLOCK_MHZ = 100;

    /* Generates clock. */
    initial begin
        sim_done = 0;
        clk <= 0;

        while (~sim_done) #(500/CLOCK_MHZ) clk <= ~clk;
    end

    /* Generates reset. */
    initial begin
        reset <= 1;
        repeat (10) @(posedge clk);
        reset <= 0;
    end

    /* Tracks data that has been provided to the hit assembler but has
       not yet been assembled and output by the hit assembler. */
    logic data_queue[$];
    integer output_blocks_consumed;

    task provide_din (input logic data[]);
        // Add data to queue for output consumer.
        data_queue = {data_queue, data};
        
        // Set input for hit assembler.
        din <= {>>{data}};
        din_size <= 7'(data.size());
        din_eos <= 1'b0;
        din_eoe <= 1'b0;
        din_dv <= 1;

        // Let assembler receive data.
        @(posedge clk);

        // Clear data valid so if we keep clocking, the hit assembler
        // doesn't spuriously consume data. A subsequent call to this
        // task can set it again.
        din_dv <= 0;        
    endtask
    
    task provide_din_random (input integer size);
        logic data[];

        data = new[size];

        // Generate some random data.
        assert(randomize(data));

        provide_din(data);
    endtask

    task provide_din_ones (input integer size);
        logic data[];

        data = new[size];

        // Fill the array with ones. If there is a better way to do this, please let me know.
        for (integer i = 0; i < size; i++)
            data[i] = 1'b1;

        provide_din(data);
    endtask

    task provide_din_zeros (input integer size);
        logic data[];

        data = new[size];

        for (integer i = 0; i < size; i++)
            data[i] = 1'b0;

        provide_din(data);
    endtask

    task automatic expect_output_blocks (inout integer blocks_expected, input integer amount, input integer timeout);
        integer cycles;

        cycles = 0;
        blocks_expected+= amount;
        
        while (output_blocks_consumed < blocks_expected && cycles < timeout) begin
            @(posedge clk);
            cycles++;
        end

        if (output_blocks_consumed < blocks_expected) begin
            $error("timed out before receiving an output block");
            $stop;
        end

        if (output_blocks_consumed != blocks_expected) begin
            $error("received too many output blocks");
            $stop;
        end
    endtask

    task automatic expect_no_output (input integer blocks_expected, input integer timeout);
        integer cycles;

        cycles = 0;

        repeat(timeout) @(posedge clk);

        if (output_blocks_consumed != blocks_expected) begin
            $error("received an output block when not expected");
            $stop;
        end
    endtask
    
    /* Inputs. */
    initial begin
        integer blocks_expected; // cumulative
        blocks_expected = 0;
        
        restart <= 0;
        tag_init <= 0;
        chip_id <= 2'b10;
        en_chip_id <= 1'b1;
        en_eos <= 1'b1;

        din_dv <= 0;
        ready_in <= 1;

        @(posedge clk);
        /* Wait for reset to complete. */
        while (reset) @(posedge clk);
        
        /* Wait for DUT to be ready for input. */
        while (~ready_out) @(posedge clk);

        /* Since we are starting a new stream, the hit assembler will
         insert some headers. Add this to our queue so the readout doesn't
         get confused. */
        {>>{data_queue}} = {chip_id, tag_init};

        /* Try feeding in a little bit of data piecemeal. */
        provide_din_ones     (8);
        provide_din_random   (44);
        expect_no_output     (.blocks_expected(blocks_expected), .timeout(100));

        /* Make sure each block finishes exactly on their boundaries. */
        
        provide_din_random   (1); // exactly enough to finish out the first block
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));

        $display("Finished out 0");
        
        provide_din_random   (63); // exactly enough to finish out the second block
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));

        $display("Finished out 1");
        
        provide_din_random   (63); // exactly enough to finish out the third block
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));

        $display("Finished out 2");
        
        provide_din_random   (63); // exactly enough to finish out the fourth block
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));

        $display("Finished out 3");
        
        $display("Each single block finished out correctly exactly on its boundaries.");

        /* Make sure that we can finish two blocks at the same time, exactly on their boundaries. */

        /*     Even pairs */
        
        provide_din_random   (40);
        provide_din_random   (86); // finish out the second block and first block simultaneously
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(2), .timeout(100));

        $display("Finished out 0+1 simultaneously");
        
        provide_din_random   (40);
        provide_din_random   (86); // finish out the fourth block and third block simultaneously
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(2), .timeout(100));

        $display("Finished out 2+3 simultaneously");

        /*     Offset 1 block for odd pairs */
        
        provide_din_random   (103); // wrap around, finish the first block and start the second block
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));

        $display("Wrapped around to finish 0");

        /*     Odd pairs */
        
        provide_din_random   (86); // finish out the third block and second block simultaneously
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(2), .timeout(100));

        $display("Finished out 1+2 simultaneously");

        provide_din_random   (40);
        provide_din_random   (86); // finish out the first block and fourth block simultaneously
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(2), .timeout(100));

        $display("Finished out 3+0 simultaneously");

        /* Make sure blocks don't finish early. */

        assert(dut.next_shift == 63);
        assert(dut.next_wr_fbuf == 1);

        provide_din_random   (62); // one bit short of completing the block
        expect_no_output     (.blocks_expected(blocks_expected), .timeout(100));

        assert(dut.next_wr_fbuf == 1);
        
        $display("starved 1");

        provide_din_random   (63); // finish one block but not the next
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));
        expect_no_output     (.blocks_expected(blocks_expected), .timeout(100));

        assert(dut.next_wr_fbuf == 2);
        
        $display("finished 1, starved 2");
        
        provide_din_random   (63); // finish one block but not the next
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));
        expect_no_output     (.blocks_expected(blocks_expected), .timeout(100));

        assert(dut.next_wr_fbuf == 3);
        
        $display("finished 2, starved 3");
        
        provide_din_random   (63); // finish one block but not the next
        expect_output_blocks (.blocks_expected(blocks_expected), .amount(1), .timeout(100));
        expect_no_output     (.blocks_expected(blocks_expected), .timeout(100));

        assert(dut.next_wr_fbuf == 0);
        
        $display("finished 3, starved 0");
        
        #1000;

        sim_done <= 1;
    end

    /* 64-bit Packet Readout. */
    initial begin
        logic [62:0] expected_block;
        logic        is_first;
        
        @(posedge clk);
        while (reset) @(posedge clk);

        output_blocks_consumed = 0;
        is_first = 'b1;
        
        while (~sim_done) begin
            while (~dout_dv && ~sim_done) @(posedge clk);

            if (dout_dv && ~sim_done) begin
                if (data_queue.size() < 63) begin
                    $error("Readout got block when there shouldn't have been enough data to produce a complete block.");
                end

                /* Pop 63 bits off the queue. Bit ordering is kinda screwy here... */
                expected_block = {>>{data_queue[0:62]}};
                data_queue = data_queue[63:$];
                
                $display("BLOCK: (EOS %b EOE %b)", dout_eos, dout_eoe);
                /* The "expected" line is intentionally off by one because the first bit of the readout is
                   the "new stream" bit and should not be checked against queue contents. */
                $display("Readout:  %b", dout);
                $display("Expected:  %b", expected_block);

                assert(dout[63] == is_first); // new stream
                is_first = 0;

                assert(dout[62:0] == expected_block);

                output_blocks_consumed <= output_blocks_consumed + 1;
                
                @(posedge clk);
            end
        end
    end
endmodule 
