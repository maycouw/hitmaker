//----------------------------------------------------------------------------------------------------------------------
// [Filename]       hit_addr.sv [RTL]
// [Project]        RD53B emulator
// [Author]         
// [Language]       SystemVerilog 2012 [IEEE Std. 1800-2012]
// [Created]        
// [Description]    Encode the address section of hit data (qrow, ccol, is_last, is_neighbor)  
//
//----------------------------------------------------------------------------------------------------------------------
`ifndef HIT_ADDR__SV
`define HIT_ADDR__SV

`timescale 1ns / 1ps

//----------------------------------------------------------------------------------------------------------------------------------
module hit_addr
(
    input   logic           clk         ,
    input   logic           reset       ,

    input   logic   [ 7:0]  din_qrow    ,   // 
    input   logic   [ 5:0]  din_ccol    ,   // 
    input   logic           din_eos     ,   // Input data end-of-stream. Used to flush hits
    input   logic           din_dv      ,   // Input data valid
    
    output  logic   [15:0]  dout        ,   // Output data.  
    output  logic   [ 4:0]  dout_size   ,   // Number of bits in dout (2 to 16)
    output  logic           dout_dv         // Output data valid
);

    //--------------------------------------------------------------------------------------
    // Input is ccol and qrow
    //
    // Output can have ccol, is_last, is_neighbor and qrow
    // qrow and/or ccol can be omitted, depending on the setting of is_last and is_neigbor
    //
    //  Full output   ccol[5:0], is_last, is_neighbor, qrow[7:0]    = 16 bits
    //
    // If is_last is set then ccol is included in the NEXT hit output.
    // If is_last is zero then ccol can be omitted from the current hit output
    //  Reduced     : is_last, is_neighbor, qrow[7:0]               = 10 bits
    //
    // If is_neighbor is set then qrow can be omitted from the current hit output
    //  Reduced     : ccol[5:0], is_last, is_neighbor,              =  8 bits
    //
    // If is_neighbor is set and is_last is zero then qrow and ccol can be omitted from the current hit output
    //  Reduced     : is_last, is_neighbor,                         =  2 bits
    //
    //--------------------------------------------------------------------------------------

    logic           is_last;
    logic           is_neighbor;

    // Pipeline regs
    logic   [ 7:0]  qrow_d1, qrow_d2;
    logic   [ 5:0]  ccol_d1, ccol_d2;
    logic           din_dv_d1, din_dv_d2;
    logic           is_last_d1;
    logic           din_eos_d1, din_eos_d2;

   
    //--------------------------------------------------------------------------------------
    // 3-stage pipeline to match hitmap encoder delay
    //--------------------------------------------------------------------------------------
    always_ff @(posedge clk) 
    begin

        din_eos_d1  <= din_eos;
        din_eos_d2  <= din_eos_d1;

        if (reset) begin

            qrow_d1     <= 0;
            qrow_d2     <= 0;
            ccol_d1     <= 0;
            ccol_d2     <= 0;
            din_dv_d1   <= 0;
            din_dv_d2   <= 0;
            is_last     <= 1;
            is_last_d1  <= 1;
            is_neighbor <= 0;
        
        end else if (din_dv || din_eos_d1 || din_eos_d2) begin

            //------------------------------------------------------------------------------
            // First stage pipeline 
            //------------------------------------------------------------------------------
            if (din_dv) begin
                din_dv_d1   <= din_dv;
                qrow_d1     <= din_qrow;
                ccol_d1     <= din_ccol;
            end else begin      // Flush
                din_dv_d1   <= 1'b1;
                qrow_d1     <= 8'b00000000;
                ccol_d1     <= 6'b000000;
            end 

            if (din_qrow == qrow_d1 + 1) 
                is_neighbor <= 1'b1;
            else
                is_neighbor <= 1'b0;

            if (din_ccol != ccol_d1) 
                is_last     <= 1'b1;
            else
                is_last     <= 1'b0;

            is_last_d1  <= is_last;

            //------------------------------------------------------------------------------
            // Second stage
            //------------------------------------------------------------------------------
            din_dv_d2   <= din_dv_d1;
            qrow_d2     <= qrow_d1;
            ccol_d2     <= ccol_d1;

            //------------------------------------------------------------------------------
            // Third stage
            //------------------------------------------------------------------------------
            if      ((is_last_d1 == 1'b1) && (is_neighbor == 1'b0)) begin   // All fields
                dout        <= {ccol_d2, is_neighbor, is_last, qrow_d2};   
                dout_size   <= 5'b10000;                                    // 16 bits
            end 

            else if ((is_last_d1 == 1'b0) && (is_neighbor == 1'b0)) begin   // No ccol needed 
                dout        <= {is_neighbor, is_last, qrow_d2, 6'b0};        
                dout_size   <= 5'b01010;                                    // 10 bits
            end

            else if ((is_last_d1 == 1'b1) && (is_neighbor == 1'b1)) begin   // No qrow needed
                dout        <= {ccol_d2, is_neighbor, is_last, 8'b0}; 
                dout_size   <= 5'b01000;                                    // 8 bits
            end

            else if ((is_last_d1 == 1'b0) && (is_neighbor == 1'b1)) begin   // No ccol or qrow
                dout        <= {is_neighbor, is_last, 14'b0}; 
                dout_size   <= 5'b00010;                                    // 2 bits
            end
            
            else begin
                dout        <= 14'b0; 
                dout_size   <= 5'b0;  
            end

            // Pipeline delays for data valid
            dout_dv     <= din_dv_d2;

        end else begin
            dout_dv     <= 1'b0;
            dout        <= 14'b0; 
            dout_size   <= 5'b0;  
        end

    end 

endmodule
    
`endif
