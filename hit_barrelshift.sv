//----------------------------------------------------------------------------------------------------------------------
// [Filename]       hit_barrelshift.sv [RTL]
// [Project]        RD53B pixel ASIC
// [Author]         
// [Language]       SystemVerilog 2012 [IEEE Std. 1800-2012]
// [Created]        
// [Description]    Component of the block that builds 64-bit Aurora output words 
//                  from variable size hit data.
//
//                  Barrel shift the 110-bit input right, into a position in an 
//                  output register that can be read out in 63-bit blocks. 
//                  Bits shifted below bit 0 are wrapped around to
//                  the upper bits. (Right shift with rotation)
//                  This requires the output register to be 4x64 bits so that the 
//                  max 110 bit value can be written without LSBs wrapping around 
//                  into the same word as the MSBs for 2 clock cycles.
//                  The output words are from reg_accum[251:189], [188:126], [125:63] and [62:0]
//
//                  Example: A shift of 94 would have din[110:0] shifted to reg_accum[xx:xx]. 
//                  This over-writes bits in three of the 63-bit segments. The segment
//                  from [188:126] is complete, as is the segment [125:63]. Segment [62:0] is not complete.
//                  If there were only three segments then instead of the LSBs written to the third segment [62:51]
//                  they would have wrapped around to the first segment and over-written bits [251:???]
//                    
//                  The required 252-bit shift is carried out in multiple stages.
//                  A shift of 252 can be made from  the following multiple shifts:
//                    12 16-bit shifts and 4 15 bit shifts 
//                    4  32-bit shifts and 4 31 shifts,
//                    4  63-bit shifts
//          or even something like 10 23-bit shifts and 1 22-bit shift
//                 
//                  This implementation uses a pipeline of 4 32-bit shifts and 4 31-bit shifts
//----------------------------------------------------------------------------------------------------------------------
`ifndef HIT_BSHIFT__SV
`define HIT_BSHIFT__SV

`timescale 1ns / 1ps

module hit_barrelshift
(
    input   logic           clk             ,
    input   logic           reset           ,

    input   logic  [109:0]  din             ,   // Hit data in MSBs (addr,hitmap,ToTs)
    input   logic   [ 7:0]  din_shift       ,   // Shift input data right, with rotation, by this amount. 0-127
    input   logic           din_eos         ,   // Input data end-of-stream. Pipelined through block.
    input   logic           din_eoe         ,   // Input data end-of-event. Pipelined through block.
    input   logic   [ 7:0]  din_ptr         ,   // Input value of the next free bit in the output once din has been stored. Pipelined through block.
    input   logic           din_dv          ,   // Input data valid.
    
    output  logic  [251:0]  dout            ,   // Output data. 
    output  logic           dout_eos        ,   // Output data end-of-stream.
    output  logic           dout_eoe        ,   // Output data end-of-event.
    output  logic   [ 7:0]  dout_ptr        ,   // Output value of the next free bit in the output once dout has been stored.
    output  logic           dout_dv             // Output data valid
);


    logic  [109:0]  din_d1; // Pipeline register for din while din_shift is being calculated

    // Pipeline registers
    logic   [ 8:0]  sreg_din_dv;
    logic   [ 8:0]  sreg_din_eos;
    logic   [ 8:0]  sreg_din_eoe;
    logic   [ 7:0]  sreg_din_ptr[8:0];

    // Pipeline shift accumulation registers
    logic [251:0]   reg_accum1, reg_accum2, reg_accum3, reg_accum4;   
    logic [251:0]   reg_accum5, reg_accum6, reg_accum7, reg_accum8;   

    // Shift at each stage (Each shift can be shift of 0 to 31 or 32) 
    logic   [5:0]   shift1, shift2;   
    logic   [5:0]   shift3, shift4;   
    logic   [5:0]   shift5, shift6;   
    logic   [5:0]   shift7, shift8;   

    // Remaining shift at each stage (0 to 252) [Could be reduced for later stages] 
    logic   [7:0]   rem_shift1, rem_shift2;   
    logic   [7:0]   rem_shift3, rem_shift4;   
    logic   [7:0]   rem_shift5, rem_shift6;   
    logic   [7:0]   rem_shift7, rem_shift8;   

    

    //--------------------------------------------------------------------------------------
    // Pipeline stages.
    //--------------------------------------------------------------------------------------
    always_ff @(posedge clk) 
    begin

        if (reset) begin

            sreg_din_dv     <= 0;
            sreg_din_eos    <= 0;
            sreg_din_eoe    <= 0;
            sreg_din_ptr[0] <= 0;
            sreg_din_ptr[1] <= 0;
            sreg_din_ptr[2] <= 0;
            sreg_din_ptr[3] <= 0;
            sreg_din_ptr[4] <= 0;
            sreg_din_ptr[5] <= 0;
            sreg_din_ptr[6] <= 0;
            sreg_din_ptr[7] <= 0;
            sreg_din_ptr[8] <= 0;

        end else begin

            sreg_din_dv     <= {sreg_din_dv[7:0] , din_dv };
            sreg_din_eos    <= {sreg_din_eos[7:0], din_eos};
            sreg_din_eoe    <= {sreg_din_eoe[7:0], din_eoe};
            sreg_din_ptr    <= {sreg_din_ptr[7:0], din_ptr};


            //------------------------------------------------------
            // Stage0   (No shifting of data)
            //------------------------------------------------------
            din_d1  <= din;     // Delay din while shift1 is calculated

            // Calc first set of shifts
            if (din_shift > 32) begin
                shift1      <= 32;
                rem_shift1  <= din_shift - 32;
            end else begin
                shift1      <= din_shift;
                rem_shift1  <= 0;
            end

            //------------------------------------------------------
            // Stage1 (Shift1, calc shift2)
            // Shift din_d1 and place in 252-bit reg
            //------------------------------------------------------
            reg_accum1 <= 252'bX; // a default, in case shift1 is somehow invalid
            for (integer i = 0; i <= 32; i++) begin
                if (shift1 == i)
                    reg_accum1 <= {32'b0, din_d1, 142'b0}[i +: 252];
            end
                                                        
            // Calc next set of shifts                 
            if (rem_shift1 > 32) begin
                shift2      <= 32;
                rem_shift2  <= rem_shift1 - 32;
            end else begin
                shift2      <= rem_shift1;
                rem_shift2  <= 0;
            end

            //------------------------------------------------------
            // Stage2 (Shift2, calc shift3)
            //------------------------------------------------------
            reg_accum2 <= 252'bX; // a default, in case shift2 is somehow invalid
            for (integer i = 0; i <= 32; i++) begin
                if (shift2 == i)
                    reg_accum2 <= {reg_accum1, reg_accum1}[i +: 252];
            end

            // Calc next set of shifts
            if (rem_shift2 > 32) begin  
                shift3      <= 32;
                rem_shift3  <= rem_shift2 - 32;
            end else begin
                shift3      <= rem_shift2;
                rem_shift3  <= 0;
            end

            //------------------------------------------------------
            // Stage3 (Shift3, calc shift4)
            //------------------------------------------------------
            reg_accum3 <= 252'bX;
            for (integer i = 0; i <= 32; i++) begin
                if (shift3 == i)
                    reg_accum3 <= {reg_accum2, reg_accum2}[i +: 252];
            end

            // Calc next set of shifts
            if (rem_shift3 > 32) begin  
                shift4      <= 32;
                rem_shift4  <= rem_shift3 - 32;
            end else begin
                shift4      <= rem_shift3;
                rem_shift4  <= 0;
            end

            //------------------------------------------------------
            // Stage4 (Shift4, calc shift5)
            //------------------------------------------------------
            reg_accum4 <= 252'bX;
            for (integer i = 0; i <= 32; i++) begin
                if (shift4 == i)
                    reg_accum4 <= {reg_accum3, reg_accum3}[i +: 252];
            end

            // Calc next set of shifts
            if (rem_shift4 > 31) begin  
                shift5      <= 31;
                rem_shift5  <= rem_shift4 - 31;
            end else begin
                shift5      <= rem_shift4;
                rem_shift5  <= 0;
            end

            //------------------------------------------------------
            // Stage5 (Shift5, calc shift6)
            //------------------------------------------------------
            reg_accum5 <= 252'bX;
            for (integer i = 0; i <= 31; i++) begin // shortened to 31 bits
                if (shift5 == i)
                    reg_accum5 <= {reg_accum4, reg_accum4}[i +: 252];
            end

            // Calc next set of shifts
            if (rem_shift5 > 31) begin  
                shift6      <= 31;
                rem_shift6  <= rem_shift5 - 31;
            end else begin
                shift6      <= rem_shift5;
                rem_shift6  <= 0;
            end


            //------------------------------------------------------
            // Stage6 (Shift6, calc shift7)
            //------------------------------------------------------
            reg_accum6 <= 252'bX;
            for (integer i = 0; i <= 31; i++) begin // shortened to 31 bits
                if (shift6 == i)
                    reg_accum6 <= {reg_accum5, reg_accum5}[i +: 252];
            end

            // Calc next set of shifts
            if (rem_shift6 > 31) begin  
                shift7      <= 31;
                rem_shift7  <= rem_shift6 - 31;
            end else begin
                shift7      <= rem_shift6;
                rem_shift7  <= 0;
            end

            //------------------------------------------------------
            // Stage7 (Shift7, calc shift8)
            //------------------------------------------------------
            reg_accum7 <= 252'bX;
            for (integer i = 0; i <= 31; i++) begin // shortened to 31 bits
                if (shift7 == i)
                    reg_accum7 <= {reg_accum6, reg_accum6}[i +: 252];
            end

            // Calc next set of shifts
            if (rem_shift7 > 31) begin  
                shift8      <= 31;
                rem_shift8  <= rem_shift7 - 31;
            end else begin
                shift8      <= rem_shift7;
                rem_shift8  <= 0;
            end


            //------------------------------------------------------
            // Stage8 (Shift8)
            //------------------------------------------------------
            reg_accum8 <= 252'bX;
            for (integer i = 0; i <= 31; i++) begin // shortened to 31 bits
                if (shift8 == i)
                    reg_accum8 <= {reg_accum7, reg_accum7}[i +: 252];
            end
        end
        
    end 

    assign  dout        = reg_accum8;
    assign  dout_dv     = sreg_din_dv[8];
    assign  dout_eos    = sreg_din_eos[8];
    assign  dout_eoe    = sreg_din_eoe[8];
    assign  dout_ptr    = sreg_din_ptr[8];


endmodule
    
`endif

