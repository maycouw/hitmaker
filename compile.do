vlib work

# Main Sources
vlog "./enc_2bit.sv"
vlog "./enc_8x1.sv"
vlog "./event_gen_incr.sv"
vlog "./fifo_512x66b_dpram.sv"
vlog "./fifo_assem.sv"
vcom "./fifo_NxMbit.vhd"
vlog "./hit_addr.sv"
vlog "./hit_assembler.sv"
vlog "./hit_barrelshift.sv"
vlog "./hit_checker.sv"
vlog "./hit_encode.sv"
vlog "./hitmaker.sv"
vlog "./hit_packer.sv"
vlog "./hit_tots.sv"
vlog "./simplePRNG.sv"

# IP Sources
vcom "./ip/dpram_512x66b/dpram_512x66b_sim_netlist.vhdl"
vcom "./ip/fifo_chk_512x32b/fifo_chk_512x32b_sim_netlist.vhdl"

# Testbench Sources
vcom "./testbenches/hit_decoder_test.vhd"
vcom "./testbenches/tb_encode_decode.vhd"
vcom "./testbenches/tb_event_gen.vhd"
vcom "./testbenches/tb_hit_addr.vhd"
vcom "./testbenches/tb_hit_encode.vhd"
vcom "./testbenches/tb_hitmaker.vhd"
vcom -2008 "./testbenches/tb_hit_packer.vhd"
vcom "./testbenches/tb_hit_tots.vhd"
vlog "./testbenches/tb_hit_barrelshift.sv"
vlog "./testbenches/tb_hit_assembler.sv"