//----------------------------------------------------------------------------
//-- FIFO : Common clock, Fall-through, full, empty, level outputs
//-- 512 words x 66 bit 
//-- Built using DPRAM IP
//----------------------------------------------------------------------------
`ifndef FIFO_512x66B__SV
`define FIFO_512x66B__SV

`timescale 1ns / 1ps

module fifo_512x66b
(
    input   logic           clk         , // 
    input   logic           srst        , // 

    input   logic  [65:0]   din         , // Input data
    input   logic           wr_en       , // Write

    input   logic           rd_en       , // Read
    output  logic  [65:0]   dout        , // Output data

    output  logic           full        , // Set as '1' when the FIFO is full
    output  logic           overflow    , // Set as '1' when write to full FIFO
    output  logic           empty       , // Set as '1' when the FIFO is empty
    output  logic           underflow   , // Set as '1' when read empty FIFO
    output  logic   [9:0]   data_count    // Number of words stored in FIFO
);

    localparam C_DEPTH      = 512;   // Depth of FIFO
    localparam C_WIDTH      =  66;   // Bitwidth of FIFO  
    
    logic   [ 8:0]  ptr_read;
    logic   [ 8:0]  ptr_write;
    

    //----------------------------------------------------------------------
    // All logic in one process
    //----------------------------------------------------------------------
    always_ff @(posedge clk) 
    begin
        if (srst == 1'b1) begin

            ptr_read    <= 0;
            ptr_write   <= 0;
            data_count  <= 0;
            full        <= 1'b0;
            empty       <= 1'b1;

        end else begin

            // Read
            if (rd_en == 1'b1) begin     
                if (ptr_read == C_DEPTH-1)       // Wrap read pointer.
                    ptr_read    <= 0;
                else 
                    ptr_read    <= ptr_read + 1;      
            end;

            // Write
            if (wr_en == 1'b1) begin     
                if (ptr_write == C_DEPTH-1)      // Wrap write pointer.
                    ptr_write   <= 0;
                else 
                    ptr_write   <= ptr_write + 1;  
            end

            
            // data count
            if ((rd_en == 1'b1) && (wr_en == 1'b0)) begin

                data_count  <= data_count - 1;
                full        <= 1'b0;

                if (data_count == 1)
                    empty       <= 1'b1;
                else
                    empty       <= 1'b0;

            end else if ((rd_en == 1'b0) && (wr_en == 1'b1)) begin

                data_count  <= data_count + 1;
                empty       <= 1'b0;

                if (data_count == C_DEPTH-1)   
                    full        <= 1'b1;
                else
                    full        <= 1'b0;
            end 
        end 

    end


    //--------------------------------------------------------
    // Block RAM IP. Simple dual port with no I/O registering
    //--------------------------------------------------------
    dpram_512x66b   u_dpram_512x66b
    (
        .clka   (clk        ),      // input  wire 
        .wea    (wr_en      ),      // input  wire [ 0 : 0] 
        .addra  (ptr_write  ),      // input  wire [ 8 : 0] 
        .dina   (din        ),      // input  wire [65 : 0] 

        .clkb   (clk        ),      // input  wire 
        .addrb  (ptr_read   ),      // input  wire [ 8 : 0] 
        .doutb  (dout       )       // output wire [65 : 0] 
    );

    assign  overflow    = wr_en & full;
    assign  underflow   = rd_en & empty;

endmodule 

`endif

