// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
// Date        : Sat Apr 17 00:54:26 2021
// Host        : Squash running 64-bit Arch Linux
// Command     : write_verilog -force -mode funcsim
//               /home/misson20000/sync/academic/uw/research/hitmaker_sources/ip_vivado/dpram_512x66b/dpram_512x66b_sim_netlist.v
// Design      : dpram_512x66b
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7vx485tffg1157-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dpram_512x66b,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module dpram_512x66b
   (clka,
    wea,
    addra,
    dina,
    clkb,
    addrb,
    doutb);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [65:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [8:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [65:0]doutb;

  wire [8:0]addra;
  wire [8:0]addrb;
  wire clka;
  wire [65:0]dina;
  wire [65:0]doutb;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [65:0]NLW_U0_douta_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [65:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "1" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     7.0365 mW" *) 
  (* C_FAMILY = "virtex7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "dpram_512x66b.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "1" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "66" *) 
  (* C_READ_WIDTH_B = "66" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "NO_CHANGE" *) 
  (* C_WRITE_MODE_B = "READ_FIRST" *) 
  (* C_WRITE_WIDTH_A = "66" *) 
  (* C_WRITE_WIDTH_B = "66" *) 
  (* C_XDEVICEFAMILY = "virtex7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  dpram_512x66b_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(NLW_U0_douta_UNCONNECTED[65:0]),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[65:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 30384)
`pragma protect data_block
L93Lt2eiWpwbpqcwaNudTDkAhGNbBH8lxPQnSY8Arp0T8Vqu1ERSWdq65Osmo8c5HFM4RZTy+TUp
rWCeo0cHWUbKgIrliXuCud+tVOQ/dZT/7atDGXTpTgWOMwJzFWG7NSwfxfqGCvMWsKqwWVoyWfIk
STHxBWXRWdZlk080r9tizDass+8xCCDuq2rBmqqyXMeCbCcpIAapp4EXs+xGYtDxPkW2n4l0tMFb
DKgcihJXJa6uev+WyXVRBC4airqzHQAfvBjxLQI8cmv60iQOIldAKGjq9foArFW23/0H3b9QYy7G
sbnOlbi+HAgW79N6YwpnQfK6+gKYrgnPOQQksqZ30bDcPJt8xFzbAjof7VoTN/s5NAGeB6yZkgPo
BYBVyKRGnYVxdeTcMIVZEP9uzbMSMhD1nqSOMRabPfR8YrvGdXCBMpQQnffEW2HvG3PxoysEgttg
b2TxID20m5Kzc7R/XuJFnqmcX5BnkH4TVfAs5WmbtG0i9wE7JpVHFPITTrUsWbqMsTpRAj4d/Ca/
HMub1DMgHqphSL3kmt9Y9WxzywmEGcbrpuODJLepAjVuWC8z61UIGawuJYeXVMPvsZMIAEiyDV/k
6Rkbu9u4idueMmYd729JIZ0Im56stMgLkKu+SzJNnVzisELYFefOy4+fE/xW5wlfJjxOnm5X71CP
SUX7TlqKfM9AbB7HBohsYo+NQEGVUh94wf6oiMVG7vblISclVVO1TWQoF7kGMKQ3TzEA7iwiG5Ro
KgjFSvb7YqzFwy+52NUEx+0RzoWF0bcaW441jSZDy2nHT7TOPj0b2l+RdFaDgAQDE5x5+OTkM/nK
4KNmq2rMal93H40/FIKziaZiiJ0mAf6V+7jcWZIG7ytG/ZlLpd1+U/NDRVvTzHaQsIBqrbvdIjkY
nScFFjelQtTjGpU0Xc94fon8xGkUOQizPKW0MzNI3RTzAeYwgq9pwQPIWLaWplc7wE9bqXxthWz8
5mLSCUywFVcF2ZK6+iAyOer8YBIaaAtcCmx2H3+nUaD6XUIiJ8cpwIgQVsXRMg0+AhhYZkPCqrHJ
ajpZVG+i9P/7Dz2gcTuOsLJJdIfW/x5RjlX5sh+sdLWTWmRriLui7CAQOeeXVhQCSdXRzPoMQe6k
TkHlzjnsa5m9nsewbX/OHvZurwEXlosD0kTFdcvdirdHPASjWP470aqB6+lNQE4qt09aJmcz3mIq
dClBat7gE4mkHgP8FwN2UMPdgRyC7uztTWhHocIQpwjvtZ+ZDCePl33OD1dxYj3c7PEfG5oHcw5t
flShpkEMhUSYXHuAezWvP707ZLh3eqQ1Oyz+a4Oq0QVZrXf9tjeK/0POnHOlEWFrSuRJ7JNJhF9x
Abhet9znwTmYnNWMY+hDfiXximmak7JCyB16Pe1KtD9AIySyZ9DtK0YQGKsap1s6KRPBORR8RgfJ
29eoy+rOD/ZYRk/CNn2Kzzj+1YNAV35zEolnthR/S48l/uNrGZAXshEdOxi3SvI7cbxL2/DLAAwU
ZhxQ3H3LnPEF1HbPRkFs6AW619FgPuM3ZGAa4KHECAf1dzW37LBMeu9OhvSniLvrjUjv8s8Bpsfi
u+Z2HGcnbpRYWV1whm6LUqhz3Pruqlpk60psgqgAK9s1Qi+WcDgx5n1EDXC6v1lOFsyBbrl9KPFb
loUY5dqMhVEVuOWTbTeQYwSs4yt79gC34B7EfFj+99ZHoh69JPIm0JHsJrbGHtOwgwepDhBcapsU
K8QwOjERXhlp+Pjg99okmrK7/BaqztjkRedmNyYUb+NzCegGOwyJPGgbKxOdjCoin/nq4WUZzFSi
CNbAggsE3l42BtbjiUjw6CnardEaIbzMp56J9gERQgE/2dMI+NyPH4nxeLFmeZ72Y/LspWL7dSHp
O0+tNsQpW/X3NSBC9KCo4URq2mRatkIyLyitHV0lh+i3VfSUaxaK8ibT9oKiQ5DDrjOV56up8OQn
ZBlOxtEWA+D6vjkUV9u4fuJfZ5P+Pwg9nxb0BWgxgE4K1yrKi4fSFYRfmDiOZ/dodbhGTYBxoevc
ZzUNbjlpr15t/JzXXJOq6aGTbJWRiDbjK9lHHbPxOAJ7rRPcPSmBfQOvs/iSoozyJEW44KHtP4H0
6lKMRlNivA40ybRMPTigvCsn2IC+WkN6jXZHyGNK7kjG6/DctV3WcW+2cKLbsbH6WSX5q+Et+3aU
ThKBZ5hYraEMlJijgCw0l0xT7oXWHY/pYfFzEN4DiOasHd1HF5Zw5EfMuApmJem5IihvdDC4Y+c7
o08Zg/yxpEm7GEufQxmcjlyTOFJz334orgKBx89M7GpHiCaC/yQ2FGXlHll72+HlLXX9BvgS4MKJ
hF+SGkuVqlnY4ZfggCve1zOhBX68eUzNQKAw/QbtifiN9AKBB/1xfD6WLJQjebR80QjDoCgo6MS/
KSdQ+PB5tbmGQ49wzoZCiSbWrNETAqr1hR6Mku89FtNJ7wg5GZfX48jmPaC28A+gnpzE9YR5y+Rz
q9S3uqEX18DjKo7cMrINxnjW9ZnBu6qIw3I49Cw9ND/Czozab/DyMT2azD4SFbF3iBTxduxif1+I
fQXXBaRcC+2to6/9vsJVTLEHB6nys1Z4SKfukA7njA80MiPFeOl4D8PPoZKaAkGNwUwQvbtu3t0p
pjxcRaMww8+uX7ZXrhPC+mOoAbXz2tdyfzjd8PNqtdYscZuf0s/kekYxhCtCWHoaqpC3Bn33v3Vm
DYqWiIXNNFkr6DKah4sDg+V2AnZ/qkNIBM2uAw0Vv72eZsziBygBMWARRYGda34PChBqwJW575PM
plHDJOiX0tR3JNjELAYetq1FD6iPZsgqRHGVf9kjetmielZ9p+FVt7yRL/igYc1+7eIxGQcrJaKK
8SXyZ8VL+CmvWk3dktKKdlFfglAAL6p6Bv7ugTsc1Y216MxHnbY9P5pdqFlvLEzedWVgVdv9JyOL
UF3b+FXof/d6ISpW00tmcy702ErmvCAKb59RQEDQhw0cDyJdCciF5o1fafFyUoFq4KiATWV6fXPY
Mkw69l7TcL3wYt9Zd0Yq3M20S9HcwPFJtaauln5lZ7Yjjv6ljz4EhwYm6ALbuqyKo084LkjSoMpI
JsytqyJHRxEIO++Q3uEo+rQH8+KIfbPgr+UTRFmeE8pFGE3Euk3PRBPi0zEc0201ybR++WuV+YY8
pgovFuXKwxxqF10MWHd/VGToOnArX1F3z8XnRIaRqdHdncVbW+tZQtQ7L3fE+LVOu4Qe4Bo4/044
X73YoLhOQOET035GW8dw4Twan3OXIhqR+ZiqAiQbCbiEgI4XPSrNulliOOVSO+1qCYHjSdwv1LnN
DlKdLXyX9KSR+IOpfMci4pdjj8o2rS26xDTTmlcYyj3ApTQil7oF/RjD/kdHiDQFLL0bEi8oG6bn
pVcJg40wl/CiQpI+nEci0eDy660E2NFx0IVsXbxrIllNLhXq0Iy/N6H/eq0iC6e51i+PUXonuT+l
22aWmgWpSACHEa46hGOpdpxDy4WBjFq1L0Lo0L3S7d+Lb0Jtt8UOAaxfdbevNDb3EKzGHhpA7D7r
+ePgKJX6yXakSTtWozvxjGxJ832nqlq3pd+ID25MvVO1QK3lqJIsdCh15d1itFXU+2UN4ctzgEvF
vHCYy+nmwvis+lt/KEBDgbH4EAIfh5lISBKXwfu8qPD9wZJdHoBwM0rwYrtWaBqtR7we3gXy6Ybu
rgmNco5w1qvce6HcDkxIyE5morsZ41EGvSD/kX7fxa9OXLh5bvizqV5njGa4Egw2hO4V57JXHWJt
ckTo3dQMzLfwcKtDaDU1ZcaykAIhbOmJ2DuDQuxYxXFqgq2Qlk0NM+fkGD+s+FVpsiU7y25LLWJU
uigPOgxkrddgUwZ/h83EJTda0vJ1HmU69flvxSU3XBALAghoXN3j7L/e2UP08ljxlTlqNAPWn9in
/72OHHwkO2ewRWF2+34M5pNIuUVA+A94d11nLtTYcK0tBG8fsF5LhEP9Ys9HGhRMrgZ31a9DW+At
CZ8+41dEyjOv/qtlK9/Ev06IgFFar46GXclYyltte+4DugXQ+v9CcaiTj4oKQn0miJTQefVjaogb
nICtJfEC/Mr63/tHWvtIuTRQltlAGSOSo/ipUd8rryGYBq4W5C62Tr6wmhXD3jSYPRvyO4aLCfr6
Dk+dk70tfAlTMHBSi7mtY/dLq7RmlbgduOxBKyz6rdz+trXEjyXezp9ViRJa8xeq6ASt9IdriX25
TW3btVp9yQ8I3KwG1z4K3aqkulmSssL1xu0zYbJI/ZYwhw8MdfA8xsc0IWMO0fZBVXw0AwHlMI7g
+wiD6fg2bcm0lU+M1Ir3CgZufpUnELml7+ggBPT14uwYrnZCh9kYwK/aF5rbwWJdoEYqQnYk81kb
wd2vZMa2MRNT+eypnQrY094veQ6ZDZYcEDNJQSOixkkXrQRW3hfmns0mbiiKaB5I3MTX0qLDS/L+
vcS8dai/kfdS4+VevXyMd/PQiDyXei9c0fhAFMBc3okeF1ZH/unAMfbHYDvxSkeQ0lEPRFIH2REb
2MBH4ZR0/uH0iQWJgWUYmG2jUKwyrlxgGnvXS8iHS173oeUsM1QNBhncfUnEskjD3XbeY/p/AwWZ
X6F4XAFAMf7oFj765JTpXIkUJU4VxUjIjAGuU3mBTHx6yamZuSj1+ZO3DqsHBJmtl+SQE3oddEqC
wnCnhks7RBSZrTDjm9luk7oudxNoWif200i6otRhkw6Mj8sQ6ESWuehMm18/faaB5LvSjGLL2FsI
LeQnSN+Ddsq6ab1bYx05ahw+VMU6wJh1++917s+B/on+DO1+0WKr2BAIO9ZwMy8ASIl7RBLJ9ZD1
yFS6iWOMlEAYFZhGNeiwuKG39nc0bfxkDv+JN0ayuMkdGRAbgo+RM1q2M09adPr3fWT0fzW3+XTI
W8wltYnSzO1S9+0N0nvW/rarw82giAwD+FEitdT5rvNKB0u0Qm5x1NEt3v55COj9UgM0cYNiWq3l
Y+E4a5NHpc4M4+NThReh+bo3103ivMdCRRUSt0f8WFUsWTNHgwYzpcTR06N8fGLOsB3pwt5v2rPU
oPkKVBJQNQJrX8GRBeOC6sCjI2o7spTrLaiD9sM+Zr4roMxzyZJIg9lfvY7OkCguonZLuRaXe8Vp
Zrirp2G+XYuxCpki0sR7zBTvaUH0YYjEqfrzvNGzES2AcxcLyCoJjrZ4FpRvJoNtZZ2EvJkRS63K
VSeGCmQDfeu3JW0RI1xy/TTfIVp0B0fZ6VDBDjouMLYlEoUPPixt2ftY4QBjeqphb2xNdAifvkKb
dvNkUTR5ZDLSZ8xr3Fl/AIcFg4wjhnVVJPx6yHBtnC48gpJ2RgP4O/hMT+XD+Ss8aeVO4221K78v
CVpPBruZ78Z2dt8cAjQMwEHAxCjVXqRk9RrXnsX66Bal6V2ej6ipczWS518j6KyzAs6qbIV6ji3T
kmjJsovzyY0TzK9treLI1O31yZZ9919BPev14dCU5aU3JXaeDVz2Tsu/vWJ7xw+bWzf5ToipR+8C
47YGxcmQEE01pOzNzMLjjKTuzOfc3QqUFg4VWyfLxMmcHySog4ffLYcAJ5oZhHpRr2P2+COfMkHk
LD1T5B3APVIc110qHGRspBsD9PBNuARxXr8CDD2vFNuA6t2VN4Xip7enoJJVWpcLTKfFIQpxgmFd
FFyLaRy4+n5xCUPoUYobVva5tIQUIClEqZMIqkuSf/deQi6LGNSvTEPyWAkJeKMDekizLRhfDG87
Ah6Yt+7IqlOQGNgy0jq6BhCgPIC7bwpNsycJb04cI9MQtBsjSnYLa5Ops/erLGFKLbJ/xEPliDAB
oS0w545J79Q7sE2SbaqHnkZ1vy1W6ABLZ7tWfhlTYRXJYU/KKpUw02Qnx4ulTuoyRJGxtYzLGGSu
fkIIr8b5ZVHdrTE17yDVNClhpToxx8Yf5KaAnT1G0W7dPdHp4t8wUb1+T8PBEAoZYs7vy3OrTjdE
jRO5e+SspwtMmU9q0Y/5mcj4x51jYzSINO50+coMhgXXvibBCyeWtgp60A4F3eNPXVfBT65t4XF2
6Lbu+mxJAh8JxJ08DpxG/jXT1vMnuWAN6Dnfuc4fLMz10bYqujvInT6CtUKj2MUOY0WvpBUxTXEr
zMcCDz7bWM1CeWEdrgRJQdtsxFfGmGi9rKxQ30SFdL78oXhW9ZyGBHQHP86II162i9+l4Db0qi6r
PNH0SX9J+qDkpbPhK1AWhx+178s5Ouk6zJs7xtWWpqnLRkuFyO+MUsUDkfzwGBUTTc5Uhpa+V0Cm
CqUeqO38Z0WQRxSYIVezDePgsiYuCuLdPlG7FLJxxZCpk5E28cergTO3Nz/s50iT9yktWPmH1nig
C2OHpTyLkmwiMmlzQNyfQlr6tNaREg7G39lQKNssf3v1wilmoUwrgZ/KZMUJDflBZgK9uXrcazdl
+nYM0h6IMuN5kirpKe7hS9CAxtCdq/HY9KqmpgmIeqAw1ykXMQX7KNzNbNNjnWqoU6fD6KLQhO2N
qa2wdJTUAyop1FbqS70E/mdUeBM1YAuDskr6JbkwutuCNxU4jEKGzCCA2HhnzlfsABkKIAJapKw1
GDXwLmUNTNBtg1m3OX607D2vbM+8pcNltsPE5pn9JvHtQt9p8P77LsjSXxV44I8PYKvZXyBwpfh/
Fbr8ILPY3yrorRTKEbw0gxOC5wjNdL8jEdmJ4bsEGrkPLZi+cuuE48yY7OwP3pfj7fgIMjttuAv3
UuAFv1foECFNq9DHzohat7OvVj8578DFM6SuPjfl2oER43+G/gxFWrkScFuPTLVnMD2PHnRqcNNM
G8/7kGyNVYPdi20MozXDvWEfx5eYwfXk4DDp8mjm5DsAT7TXJZfJNuTO8iq1tFFMQykrCG5tDjsN
snWjpL7PgjQ+BgHrcc6H7GFpx6hvXjWIkuYDp6W+Ool3DUMjBE+ww6V3gwlUbQ8HK97DvRxwIQWy
KagQLW3Mx6FzAEVG/CeEcL9nSWt6ir+JzHmy5v3lMucIe18mkNoYElcGuxQWLodhDF95TWiCG4bp
ItP8lvDH0+EB9ThhBBxDzZz2Bl74jVPckGq3LCVsky8mBhZU0bqpZ5MdX/4DqTpu1D9r1ZywHZnS
qCmjhbBeUgOregJFBjm/SR5ziLRbwaI+ZmTRSkPIP1PH82J9PPuYtS3YCZfn+H3s0lxQwzJ+ww8k
OjYadTLSWs2lpPtr4l2N1qkPF8u7Gnd004HQl6XpWcvfza/U6QmXFqB605xNm9sHyNcazBMKph8q
1BGDrO+FK/s0GqjXwWUuC0GodFAzy5vSCWjU/N2Ha4atEh8hX4tAkfWjyXCCjMyNGzZ0p29CBuun
1nigHbpng/3wDNdnKXOeKARuJsFhra2XUZKyGLdlDNf0tJ1H8ippJDoFluqSRlsOsN0Q0XRgGqGX
KdYakJJoSvXPxVG6mdOloeHUk7l84gPfnANYl/VrbZJAle+t3oPTC6Uaixeq0ZKb8D5pnT1PN669
IyB0Mf6+/Ode5hDZwjAn5u6Oa2tLg6DPxOq2LsLAgw5f5NZWt7Dw+sNpNgWVRpsUzv/Jyj3Nv3p6
Wp/f8BQ1YjW9VmqQ+Q9dw8iSO00s2VlCmpJGcyS0DitregMSWXO+mVRFTi4kecSLDGv6z1RooNSg
MMUdG0mRRckyT0rcmj7JMLp3pqKdn2xMo2ngMs7xhShEaM3kXW91m32sio8nPIy6GbMHqxgFu3Ap
PYlFTEi381DwvMRdnkiACeWzkzIBKQL9UPah2QMBFhmPCuKqDX6Uhl9p7jtHJnpSAo64yc8Iox64
BqvdUU81cn+jPHpwvga+ymg5PZ3QJsAieECzbPQg8i8sqBiO9X7YoybZUwaE9W40RUkILVXMFHTH
p4n9DW7U6Tcxa3yOAdonc8lgZEQb12U9B9VbH3JJ3HPL2Zqlc3fo9cTJe75mn+SVQqfitjwtIDDh
kbtfrdg40efxKKzEO5wPzUxTZJYVhBwe5R2ta9xO3YzMo629NiBcbbeJe9INeM6OwyfTYkeIDgNO
jqMoR4EDh8M+wIh8YBb8WTYV+Sk5xXg9p5m3sRlT0JABcdAvYYj44omFU/ksX+C89HQJv+e8p/Ux
dRmHa0BBKDeUim342L4HZFepZ07ioExBZZyGjenSc158i2gCVK+8iZpp8ITuiBAi6INN/ZIKd+cq
qBAN5+PfKL8lA3Tqreo1dYsVMLqNBXTiZAbL8JfHKW4oEfnY8WTWmUtwrdw9Fi0LyEok32weD3QF
SFj1nRn/ZFQ+ubTqoSAd5/LTw1BkNjAoOUwjesH/fnXvH53jLwnW/e433pUz4YVT4PG95kUuXEmG
uy2CK1yJP/IrW6bSFX00l9PLTuLPSnoK6WrvnqneCfwv29oyX27mXcBavyJ/hGxkv+0N53jwuFml
GJm9eMQeBSA6YN62jUlogNZo5bvqeVzDvz0sNmu8sOfTjoGf64fVGvcvA0TonecUb6GTRZoQ7rvk
x0dYKAsyx+i2rOsFdayMhQfsy5Oo7Mnjza13Gv93IWphKhS3l+8IanT29/x4Oic5gCMz25pIs/lJ
wTnP9DiRaFEPqF3NfrC8tu/3qBip9SRidPRDCV5x58VLZvlaYWyytlA5Bysq/UgWUBIdhIiFcokp
hCstrVC7Rplqs8LbSVBCX/xS5j15yH9LOHPec8W8qxREtMwg4XapGMhnE1pjCY57zvBP7Fk/rCQe
ji1D/PE1sNxU3mk3haypUEX/CyXXKkKIHPnr+IA123cMBrPwD1BiUYXpQvAGTn+LwqRAN1n/rcxk
jbR9f/PU4Kwkjb9jBV8vXB//RKxBlGIuCSx1XhUdodrQmmQBD3p4e5GiIKi7/vsn1N+NwsrSdWM1
A2BzeYGeeLq8NuqU93YaMqdR3bM55SrmhR+30Ny/VlAYWVRpXxgsXj44Y7XOxRnL87X1+4phaYDu
oBIyAm1UwDC8/s9G3w000Z18fESpLRXT26IUszJk6RxrqffHJbDSqs3DkGPQJmJP0xr/Nk5YGYjd
sj8yf+4vLRGKsICunuDDpH+eDs3Gn+hLSTE7jxzO5t7pR3WORek7yND0xLJ1wzqOB5tO/bYkkO3C
d9hTwuvUKWmy2ZyHxgo6CPmGySuOdQSwIraAiXOawgWWEGNuFydxur8D3DN/bOBegx1gG+B46xqZ
6FpdjaXk2MJgscM4yCmQu9NIaMkvZw6SA+3wIP4OL6B2m3byoRoDiPme3S/SrBMTth5Zw0N94UCs
dj19u5tnGanJd6F1uPT0D5Fj04qz6kQUPFo0c21jw1fmgFnjMeMNSI+0g2zTpqh4ypJwkygWEb7w
ajVRpKToSU6AsXqHzrETCF1CnXvmCHYMN3H3N42d04vD/Hd7KeueO+DS47GYGFHAIPDw4gEENL32
yGVu1DAPlKm+rMYFeVv9X/sXyelVOz7SJtBobQh1oecz2DeNf8G0JI+nqcomAD9RMSVGxUMxz66m
OV7000YGondxTcdUTdgC/bFf3Kt9BNKw1uyQ6fDeEhim1RWcW5lfk05x4HtGCgPDETPkznh+xMCk
8wU9Y2CH2MoUWtAiYEriGnbOew7JXgh9gI5IQ+NXSotA/V7UgmUrHzgJn0+fHxJ9mksCIYOXDnWO
cMuksJwa8FB/Q+QWxFXtS3L4O1zminISY5HpHbfPEC0csI7LOWXV1VwbnlIfaKHe2b3vaLN8MjMU
HDGZA49wLxi8KTsZNXpziBTtXxniS5yAflXumk/ubUWyqDrTfs7KELLTwUx9KYMuyOVwd6juY/ub
+HmrosYg4bvL6o+TOXWa/qalG6Nl8XKHgqkO5IlmFVhUKdCAapxml48J/xq5NpXqRanr06XExwuY
K+SLklm6MA1MAcGF2c272HM0a96MLj0bEc5o5bkKoIa8Qey4kWgcCBVVJp/uHVLjCaa9LR5USmj1
cQb/4iyT0deXBYFtMH2kFv9+mvYbRMgHeezybznv2DuP7W4AamkcUBMqu2ngAxofyeJVvT3clOeR
x66tZoactgCqoPWVReELJMmMUCibksZ16QaGFIqtm5a0hdKXdIHwUMpSOzg3QL83qvEndOCvALM6
qyIg8z5ydtqKzxzR7Jllt6PhPRlouyw98JFskmZ/sJLt13AQa7IiCBT0kn9dAE655UfhFiDc0+ZY
HQBfMf7CbHQBl1uR8GuqkG6joKHF8doVO3zrQ+60iMRzgUyJcWaolS3fLxjBpIId59hcERXYnCJT
9mj/bDmaSwlvNo+XziOiFIQjI5f8OARbwagBAgX6rPZ2ZmO7L5AB7lFm0xadDUWqtE1nRP6OL5Ip
zRgT0+XQOpmFYwIuusohXibITUcmPvH6Wqs9/agVXcZaf2FkFa5UzBdlfGLE4GMiO6rgE0268bL8
xpol+9QHod+hvh0eg0Mui4MqxqpDWlCv76JgxEW2CGy5rqjWDu7U5lefJnPDo8qlLHYGJ59IHr1d
qD3aEJ/K86WVOgANefhPaznySXIa8ObXfUfQACkiRzNNvx6auxrcmEJ4P9mRMf1pWKV29znAI+r7
KOWJZCfKFYGCf4Yyyb6wEMNbBr9LKbNkbOXsl1QitW2fP66pk98/iT1q7tcz2xbxpsX1kjrSfhH7
xvZ4yjQkVxrQ6dIrgiDQuMWVBoSahr9mWSmUEoc9G4lVJ892jlw++8F6dhoiauy0t8LTvH4N71TF
5HG9WfrindYThu+L3Z0okjRVzhkFCMQ3OQpgZ6tlPvyfzsR7J421g3tP+EfbKsBfrkvL5nw9j/8W
ngTuG4LB82OS7735To32AtRjdnfkqwLji0LaOoj5SYaYEPcwDjBI9/v425kAhuZbIPpJXICyjF/n
ZjaSz0dVH/VfxrpZa7xpe29a7XeAQVmOGqATay3rwxaHCvFh6KAAZ1frczF8zViu0JkoLdU9WY0D
fL346lDW4iFIKf94Nt88LBTfbBTwzIRe57VDTgmv6zGpQK2T9wpI2UjMoucbaA2C2L36LAQpBDog
jyvJxyLHAbhLedqXimgmsDpZP+t/mQ07g+uHEFZT0y7T+N3YmeGxxx/jZoZ6kVnO/8Ewdyk2rky8
AnPusVWXXJwrTwAwARwXTfRQngQFTmLiiBqj6HK9jSFh6SGqiC857sq14YUCABHWzjPE+EyRPqTU
iyETlUYthNBEMM+vf+Ke6I2Azb/eFZXxQpJnh5hDFnifmIgRLGf6UGz26zAqzyqQgnNSo3C9LLnF
cLRrs2tuyqUKCw8w5bqcESmOrDOOJtnZJ4WC7rsqUgNDVn4ahbX9I7ccKNj+F3mLmPJE7anUkteB
5C8/w1IJBf5Y3QofFH2OCiTVRjMgRT1yd9tOpZE3xl1ZGreQdU3A1rqWkyL2RoYSPM0pB6OaKQR9
Sb5Gm9v/u92evH4qX7teUbT3zIktA//95+kafWa57aDLsSlQ+kUsZbOR4r5njUgqucNZkB+VsRrB
yGxlIUideympOXG85IlOhhc+R7MBfqwmVbLavnWHt+C1mOHUl7/t3K0xBTeqof5PM3UQItAxIx5w
nMUM3as9Q5E0kWNxmy5PR8aH/l7NjIPoXD+Ebc4h25Abid4pu62xLBMVhOfpgq84IRyTHaSUDIpQ
nR8JJ4a3RsQgM79J1sk8B6nuAUbaedTWTYdLyyEBR5MOnbvhJWrlMJRkfmpJfbP1PYUWNzjcbBpg
Tt+aw71zVI4yEfKz4lTCIyVHkvplSHwdUfuDbW3aKuntZMvPcMOaRYutNNnFuiMAdFBl5rgMAG8c
qoakvfWJCq1eDv8oJ1eCO8ZTlSefIizqi56iF+alBOC8i5bKJpEVM7zyFFaJAOmf20fBRX5VgnfJ
yiTCCtBwPe0Zk/d8GDx/qXz6kd01gCIRMG+Hpk87GNvaLYI0Rnww8cG3TI1apdzluQ9zt3wKPEjY
V8J6WbuKQJdDdb1L2VKv29PYdQlSCNE7RES1bthwo1qF4CDN4mimnUH5tDlA1PhLP0UEGcyi6goH
dHFkRS+P7UcsVUK02v5sspZbBCeKlAgyV1lutsQHrPLUDRyYtarbG6dAxtj9Y8VGA+/H5Wp1KWXU
OIBi33JktZmS/ck3BZiS+ElYXvks4NRMX+E0wsbwtPPQMFfnYTpGgezjcsY2WGlX0ooMgvhEobjH
RjG3ZsrvrsLhVfkmu2IPG6VnRUsSkqoYaUV+QG77Q+U78wdPfQgEng7UXLI0Z5dC2PTjBeFdC0uw
H0xDWaemgovI1I9nL6lIyRwZ6nGclnz8ytIMi1ViZCo+iPra/dUVNFF6S9IDgBMcNnfxE/jFPOMy
3Kfc2uUqYfie6PRyjoX1NKCF9lHXhpn8W7Ll4Siq3W6cJB4hUBQEBRwKYyKOJv7YpRIW3JjXtnQt
cyNeRmykhYeopotW17VcTbIiMcJGamcMxTCe5oT26zPbHpIJhz1mmVyDpvGUPnA4gDzIe29bDuud
qM9a7Q2WSoNSFHtP++zjtMsmrtA6sTpVKTB2GmAeBBTH1neZy13NqKJvW39jmtql1+kc7DP5EyG2
qdtZHQp9QsBfXy4ZVvu1uNAiVFvODMLUHxnC7e+8o/4vz+iAAlHO2Eo418z9k2sxEOLSQaDhVAeU
yqucut93w0E0X8hjnGC9X5rRRFYyJFiAQ+uCb0qZJonT/X3vkN/C2L6Ma73THLo8Xev0EAd4cBPH
eS1/YiQ8tpk8OhHX9ba5mAGKyPPWLWcitIQUPi+ZvGZNbTyr545LiHnoTzmhnOLa3cQaakUXqJTW
SJJJtLJ24eyYYbcuDhQA41xvnOKrP2e8cy3Sd3Ju0r+2Ow6uW/0t3OY4IhMEpdzSYnI+5xwyHYDU
PEQuzYnp0S6SukX4ekhYOFrYUzHsTBXr99+kl6XlhzYkI5PT9XmPSTPlI3ZWTAdtwHiBE01egS5R
MH18xcskkfnCJNWJJV7u4tHJGysECUH3PyuEkhl94NYlzbqpqSkH8OSJGMzdwJB6+dIUQnw7Ley6
j+qh8aP6Nxdp8L1r6jM2Q8GB88YcNRsZvPxYKcMDatNIplHMKh9+0+hDe5WPVMv1K6Q3A7lU5HDm
96qoXA3UGAa3quj/hYj88eWZGpUcDDoZS1kBW6rGYfywg//lTyPjzOYR/i6yZqcR7kyDjlzuyrYX
xK51lbNrfW+reY3GbPw5BihukuoGNuRAy642jiPN6GHJzREwAt035+owG2tqOThmVYrOBPu0JN3j
VvB391chUHgRBwwNrWPt6lmVPv20gwxM5R2h9u+naOn642H3FB2Edg1iZXHFAuNk/MAtNoMSsMFU
nRwyo8SgNWgHe9+dlpR2XamU7sLMZnXy1dk1GPg02FkodR7Hb6vPcXIos2kckg5bEi5RXo9Wc7uP
e6G73PqK/vlPra62BQDUiXjzSVI1PJWLEl3JQKc39+wy9SKA1XQyYM2Jsgc0cGJ697a8sGltbDMr
wglOfBo6Xidm9892x6kMWHbYsCszaIdHWWb03I3buQVq5NHX9ic9pPdeGJpQtFDydBSNRJNbwGOF
b5kk4SAmqKrG0FLdBNNlO/PwAydiYXk8FFuhceHXbfxrjwRzoIcM8/L/s5oP1Tvfvy/Rm9e3NVHU
C6TvEUciHeLG1QTLXzMIe9NvmB6NuP0Fhy6e1PklaJnLuzaM/vsK58EHIm50mm0XCtl88Vi5yOqp
M0aXb0FpDeUdyUIHVD+169Ju/Xy9q2s6KQDiAZaLCqBovRF6sKFDSyQvt69ozVCAWKynTPcPyyxz
n5UQveCtRQYhPnkaZo3Cc56MM7dRQ9iPyne6rvMR7WQqil4mhfG26PGgm3QvzCVfKm1iAdZc9HOP
TgN0Rod3a41FBGkctVfmP+CJ7BZrlbTDYIx19bOUln6VFPcPtVIBYg9ixjDymLlvP90+JsqvYh1h
WUXzGUf0e3xHRaXR+IPUkVJmh12wz4cBHGZx4vZTaU029giGe5IDSZUZCudd/qlKBGeD7WHNqPy6
vU74hKWtGVYU9TRAedF2jygUiWOhIT4LkWKe2czro/2XkctYM3ITswz+R4t94NblZ07eMYn9v580
QZnj4/ICNnR4y+5uT1c2OF1Cu8Gwc18mAFnZAGOgmoD+91dEm5acXMe1+N7rVFe7OK125fLw1wTV
RwtDblQdQuOQ1PTvp90+F5m9R6ML31dwpDvSvZz7zjWgRyKUEByh7Kv+aEI9sJOC8Ul0ZcKrPqQ4
ny9UmyR6yz3oyJDLf5t2Vu+M4F+5fB+HEaIecZN9RdTrAGuznv+T4AfWJI8NdtxiHdObnDAUujhx
0a+ndmla0qfSzg3Gi82lpDM9f4WxDzxKjsEbuOaMUhq2GG4uuV9EfEzUVFSQMFqumIhbZlVXhLT4
Zl+76gI9l4Es2WyUaflsmrP9tjlkjK3g8FgOwDFGgPk7MVaU3JV8gOGxhNxVCNhtfkbusb/YIVGS
vkqGEDtar+uyVA2S7DLksKjLXPwZkrgDsWMm/ZuSljbBJdXufjGtD6+ZaiXoskZhioyooRP1sRZ2
mvRcpkx56gpjKMGLw+DsgbbG1lYgi8ZBXlxUlh8pzH70Q8nbVlRvq2QAI3ggjSWhgC6LhJOSrnTl
B6h+b1/p4JzQEnboyjYZPOrkrpKuVykYli8Rgu3Y+mW62KkOpOdcs7vLvbf+4GTN8H0M40pzQXft
7AgfrTM1vfWSDnIbI2uek03hgXmfjvDkW5WejDpqihZB5Ncmc5i2kHvETojyA2w71DGmdpaxD3s2
4mUXCmoZYnzHJQHYQXSkNefjZ9iA7ePf5p5uQICaasGwVrQV6spXxY1EzSrkKEixKULEDkNeuYja
EObye5uTAv8P0jiSmatbGk9LBkblOKXdAn6/NwO8qeYmhPubokwj95D0WHGXtqTPIr+d+dKJ5e3i
ES3G4ThKgdwXmAKzX0M4T37qkLXOTUKBbn7Xj201jD6EBsbxM0KRrnWVrTUU/GCj8dikcfkFAUvP
Exf1BhW5KT3TqjjmplOn2kpL1eBhfzIOrwaAEAHtDCvsUvJv9Sv9AF4yPNpr4aVHZQIs4Nxj5QnP
bw0FcEUCpDbj4lGxCzKNJ82J4BYK6sRvqQfQH4Fg5oEGUG+P9wQbqGkqMyYtO3/t33lE8ptQc+9H
GEia949RZB/KXaHHRiC+32nCzRriqXoKcj4xPLFlzlPD1VCigoKkUSs+byDriBKpUzsTcx6e1NXg
tuzbMLe4+eWRNjs5lMay4eAdkeaGfhFOd449wCqLwofhePYYjTDYLxv28vOszrCRaujluRODfelr
S74raIe33hKyTpPxh6Ss4C+wkeuTA9aySqml1kvtrXVgWFIqYFitnOlLEj0H87W80zCah/KWKjyx
TKUfZu3T/dKmkmpRUngfwhZkzM/Xc5lH0saD6sdQyiQj9mTJqzKvOLBIC0X6JZhLfkRSxlKVzIU0
A/wdKkzaCHlelA5uE2nR+eEyHvWHbmtFeqLz8YiYT8MClWuAqAgMS5yTIKRwfMYJ0xzJW54hVuPd
Fo6Vfr4kf0YTVhNUZbCdFIJhtDCHDCkt+QiCxPnP6WnAsuM5pQ/TkB/Ms6JCjXiixNsfZ7h5V7/O
eHsJIgwqGojuPfFHUI5AGzdIAYLE1fm1AF5TJKqL6wxef4MihcNR0xuIjUjW9O2mY67u7L4sDznM
lZYANdUnldvGuTEBqMG0MUQUOphkjkee2liUlSjz4usc87XZnW6fcFeO1SCC67GSg080I3bjCgAq
NMlWBDhPCqeg/7wNf3PbXmkBx027qAA+WS7urOmxf3aF/6dLz36Uwzoq7vIeTgeQ1PhiZIeeGWJH
r0gjcXmZZwyKQMdrGClDsITYSVl7zBekNA/GDvjImmg4jZ7n4KuAiBkdXh2USw8+Utcw4IHRyqBl
wSW1O4uyK7SHN2PqViiT993KzEts64UWTSQ9LvFA3HqqLoyTbnqR/8xecGZRLZNDS9HxbbL381QU
98hbxOmcmkaQsJuN5nA787nbtQUPwivQLprakgy7pcVU9bmuFuSrDnajso97wvUxFNaLIgh67wQ7
mS/7f6p7pQQv5MWmbi+PyAGMHKoxf6L9S9Sv++yrXr8nXWmB21HKpKWtGSTzDboDopRqRYjkU0i7
wMGHrKHC63DoDSl/iY4jbvV0Iz0gtjjc55QPdUzdaG29WmdwA+nhkX5DI3HT8wdwbUa0fhY5w2AH
ViZHYhdP58DSAGhPGMsZyc+lGb+UWEF1lf5tbueb+DMUb9KFccmVyHpSLRPPijCjPvFbz5UIW3yS
sVqcWTzX87WUDDFbmpS/LYIYUn1Zopn1OVPGK5Q6KpqS4OcU5hsWq+GQ2i62nsbFMmQuG7WOGpqJ
f8X0mfo7zX/qTUxx3e7Bw9lxTzoejcYtdc2tMPT2KmSt8H8IjiPAYHaA2hir8xw2NA/GNQHw3RIf
Q5HFhgyfIAUqkdHANk9dfo3P16ilZ+vmF758WnXAP71kbJxq5PVtY0mqV6LUm89S07SQhs8j3k79
vY0zG2YRm+55o5vigHeeEctjEbGimqwKOlDPKlb7zKI4g9rs5JAtKWB6qNf5yDERjjH9q/gQPujR
uW4mX02QZBvsfF3MjlzboOJSw6m7/b3hkYAGTaHX4s6Y3TCq1WOhheciCqniXB6B9Rmmy0fZ5XlO
MM/NYlte5bxJnaW0/6xOEH+lAvonOcmHzglFX5kFgdRlkhrspP9SsIBBK+fjal9nhdKYDSn0juqa
oaYe9Pzt93LzrS7BwDaZnNyAzSHCifBtLdo/U2PdKzycMjmQ7huCqjCyVVAs0enCY4cRTBCennFH
0M44omM3lerfYK3xhYJ9ncGmCz0yjo80/udI1glhizSH6ssugpwB4Oka935RjC9c+iincT01bsOX
HBz0sMXeQEILbYtYpuFCj9FKFp6308aU34VT6O4UOH6tfBaf9wNaGodGUHdPQuwYkSDSIh0/vHm+
jQMjkP77FCT439oX7PHV591O7H51A9BDEb9y/mK04hYqVobce/mAfPFyCw43y9urXUXU1Iti8pqJ
llvzEbF42LrYfM3GS7X29plbrm6hYDWeYnJOKkYBk1+cytE+EBeUNalXeVRLsmlU5fEY0wUGIXGN
B+1BfHELdKRkRYIaI14MN33T3XeYaVjwbV/bFSQdAwr6LZYaMtj1h2VdGqJVBYNx6kvNgNZC0SHw
yhiWp/YZPA+wNqIbnZK/iwCLmzKhSx2awEZvJH7gNOGGn2KKqFKgGv8Lcgg1i4lHlNGBwO/og4iD
8plTFZKEFrpyb4z8BeNxFpXHZ1+LvFzke3kmz2/t7NAn3Ozz9CDiEtwJLwPsD8oU+ng2G212Q3lA
jr6TKsYHY19m3p8yYOMrk4D5pvpRUgfU+SYAaMXbv91/fgzjfqr9qLuct+7CMptHe76A1H/TfrIt
008COw2kQOvV9dEDR2fHRreWf4/xH4cKatwkI4Ai7OO0nQZy392U1N+18l6hbjIHySabvoUhQWu7
ddS0k2TuAxj+kgFsVJimnDZVpl0cTrJQc1VBZA7fw3AZxBwWMktHYCDIgp5bAFbKL7pN1YiwEaUG
ib42NWIKGNJhjYxzN7jo5GmcRBxh0yPpAHygihx5BImAoAZSKM3qJQZx5SMU5uLegRP9kq936ipj
K3v4vJD6n4a7wnWTd1Kiz6m+/2+edwA2dT+4f5zbgJU8JGWofV/jJTclYEBjykYL0NdwDl531DYP
b0Ae3JWhE1ONwT9S4+vQwZxHTdVlHDP1+23kS0eB+Fc3xoKRG018umFnWrljy+uiwiKSS4Kqz4aI
VdElrpZSwMOnWvveCPJOdjQtHt9PT0ON/b7fx5dnK8iy/oIwdJler24sneHwhkkdaBeF5IWIZ4x5
ZA9SW7R6vKtqYOEFLSYj6Qd7S7Y5Zo+0ubdVIITQvTEMYgwRsdQdYikJljQ62eIwiL2O5J85kRhn
YcnRLrMfYfuBQxcn3Ou9TJ7YEQAoLlwmGobbwp0a4EBSp4KsUb0Bwx3uUbIKsIOW5imv5Y15e/3J
wb/dN6vBeLKv5f0uP2qbJ7nRnnfe1owEnPa93SY+R5Z60S7QSBiuIVeundgMQKu9buZQHu2j2sRG
mmwMb4kEi3rDRzSz7oUF6tsby4qVGj4qtTKIkWws3GX7VPkuiTbYI79YV+n6ecF4f0+kva1wlOvc
Bwquaepqi/nTwHQW+DkGd4lRuAO70RvRYec4hkqmkAat7iWQUafbtC+AGyLby1X6VqK9K+1eogkX
3Cj3b4aldX7BEAnuWGZx1TCpbkmp3imzggfiaSvUJh+kfukL5y58yL9kak1CzdMMm3uy63hKO638
jKaqh1Lw066VX0vm19ROUrYNAEsPlN8Y+VlVPC0nbeYCKI54g+0aTJ3EnUU4svGze1xIVG6Ydec5
Gmv14m6/pAt+GUCz1iNT7S9DKDPxlrLd4ZMp6wOiS7TrHmpZQenDMvya/snRMvdDOvnnuqiI3OD0
jYHnRHLue4NabykysIk4erGs12viIUiuyIhUtt9GC8FTvtBw+AxYJYrs63YmX0HGEjtHntUC0aCS
82thpYiG71cuiAeHfDL0zN+tvUFlcgTV+Qlj7xOvgyhP7ifbjVeW2uBElU5Cxt190rmVYo0PtSZr
6S0O/Tl2RF4QJ3XeBGHl/FYBHfNQizK2R6+/aKXr+0TBqItL+g9v/6hw+UHa9JpZwkYnTDweDJSk
66J/O2h0LJZMNFTBO3BOIrcich2OuIaDKeA2M+WqXaqv4ajfqwNSrdE7m4YbX6QIai9ayf0GgtEK
9Iq6zGosvVD9BvbDNhunpxNQs8EzY4Gbcu4LLVBkRqfh3oqtGwhzxPxfsfqiXraFC+yIMljmQnrp
ASAPN/dEZ1qiHR2+wVYqg6cSi0/hXNE6BoC+Ht9HmEhNkTMdEwis5rJFjdPEtiD3qfhzoz5HeUkK
U8splvmEJtUw4KNMuPQCQw9t4966oZgmUeHSnBwU5nbDgX+oMdLiR7CvMyE7BWl668lOMSWZKdd1
CJ0s07eY34+WZ5oj/eEduFdh9shEeEW70kR4L8KRQhpL2Rp6ZIkMmRifvJK6dakZJhom3sY0NE2p
XhPYEq+JraxUcnN08cFpZlC6xpKsUv/KvNdw0oZ2M6Ljp+xZCnsMad8905Z1C3DWSXCkeKCB2+oj
VbyAIbcZoLRWUGUMTuowLK0ZdxIB/iwwflLtsALcouJK7y1VOe1914qHP2XJNeyMrG0W/R/kTE+3
nexx1eXaEv5+MRYzuhX2/ROH5gJSDC5rpRb4SJ3JYowIJAxb5YplJe1Dv19eTtltfT8kQNlTBdAj
/K8dUgo0XGDDurDTOqV4hXLFueYlpg0Mgqdfm5ns++/pFMOJKGgd45frIgsKuArpLu0eBrMcMIn/
pAeHZA/lxFJNcE4eKv6eMaZmDSLgTpLssEnkPnWPXxThNSGTSUyC86dmtlhbsewKy0UsILMUq3x7
+gsyGjh70f+CmIkJMJz6byDJofMzM5uwj+JVsrdzckZrdK/rnf7gBL72uiRCGnL2EkhScj1PyFPW
XNrmoeLlGtcLUyN3496e+H3aDrTpA8G4Qx/8sD8SxezBq6pbovvhxNT5XiBgZeSSbmlc2rWLzxFU
2D9TM9RW6m6R5Y6WSdU1ROumnQrsgz29Htjy3lmmGyCoYpB1cLJFuYkFMIaSuNPSplgpZ+KP9mqH
koQj+M1gpE3jwiMTu/FhPU/l69vcFzzk5a9nYLWEfsdiVWyEIYlaHgjQa++kJjthp9SRvn4PEVhw
SzYWOGpZQcts/obKxd73lVtwp4M2pmMK6O+/sWufhVwzTRD77P639AqtzxJPkHlYGDtxi+QFb1xA
d7qXl1xahEbyfJOyWTn3hU5E9F8UgvpNkb0Gmx7NA+jLHqKFxg9bBK/0d4Q8Bu5rRNmTTKSL9t7Y
am54+mHhI3Smb/hanNfunSg+YuMNIQRDIsaUQjMs5VIh9LggDQm8irTBTeSE4G7LA3A0VevinfN9
VgauAE/CBuzB/CBKtmCGrFirisBgYU17ft4VXMIb7kgvOQdrSN9EfbbIQxaHArE6sLM2/GY8S+0P
UKWiwNx8L735ExW+cDrosFbNa3mwjRlTcJQe5TML3zMf09Ox22r7CHfeyfR6XTMok5Goq2A8ydVv
2AeWFMZ2B58z9UT1eKA2WqWhYRWhaHQx8bZFpxJyzi5HygKTR+w/+9sHBDpwWIjItsTohHXWq/lA
/Ht8JJ9+iEnOYDvJ4YWdfqLnP8c1eCb1w4yN2AphX9xFNk4utq1uY5CE9GDljNwCh/N5Va2dE5An
wJp3SDXZHSLL0+KCCNn/b//0+1QZ2c4IgXVvJ4bbqF8RB8w9S+ATT5YgbDfd3EQ313vWqynVAVW5
4Y8NF1fl2ipJ+DnXdSqQhxhHfH3CAP+Jsw72Ol2/lyFiAUJ8cr/sUQNgvmqmEnj5ba9pYJn3W7gj
Roh/g+FF6NtriY1OYjIP9fqBZyIxM3Qe6wOHOG2QZ4S9UMzBQW/PWO9IieyfB68fSSggh2snsf2m
auBGFdRJGREN2M1O/41zrAzCC94eHGvCXkezFE0x1TZEHux4zbD4uoqKFdgMRootRuGePoRAc62I
aV+8EAulzZ0w5cE1Y2jBAS/qkKdOS5o6ZpKtUB5poV/swwsz5gcDjAf3fl5J7+6BwmHfbODSnMoC
XjjNKCSxQmG6bE+k/Xa17BSciZImh3Prcfa+ApoQeTbWhfS3GMHvayi1Q9vhdVWz4ut9s8Cf0/8o
mZbZJbZupn+dXpjSbkNOz79oiua2lG1EjqQTFJ2YdT6KSZzn3P8D6gacswfdbPr9PyqJk3Is2A/V
xmoA4SfMNVM5LoI27yju9Btf6VTUA5ZZzwDB5e2ou9xZLq3D9ZPYNZaYbvhTv7x4yM2YEyK3Vraw
KGsAJu5oCJuUxq1PJQlDJWrN3jTM86biECQb7BQF7OwM0a/G88QFT8wLLZELEtb3m6lcqCLZP5l5
gEA7bzzXLxUAUKeNaDJvMbCz/az/N5WCfD2l37YFsIxbm2lXmf6VkA5+C2sek22O/WYebs9VTXs7
dqtxo0EK0opCLIejCKi9qmcxUH7QpmjG1/NgE5rc8fOGEqDEbaRmQ55aDFtFh4h/LlxokE+inhAr
QiLNSiWT16m64nqKjrjYjRGh1iz/c2nGEZsljGX8bxre2EoFWZYG2YD7/iIyBRuC0XhI4eWejUZy
AlC4pzF9cVEqknb1i2CS5AY7NXx3VsrXP4QpT0hor0Zf2z5efIqd0bLlkIVWNwZ6FsFAr3qRpkHy
2jF6PZbL2/rgwITRhcKaIu75Wih7FYTZ+W6Z/wya2vKqN3z1KIdDmd397oELk9mOHQkihqAMqs79
dIXSYexDQLVN2RdAMjaI9C98SUlr0n8UAANbKu6k5cBxTGz/tGXH3ERKUp+BghXUO3jHxf0hv4b3
zCRghWWOflc0CJEzfN1BFFydxeSD8abM5BNSFv0kC6kWUI1Abn6Q5inmatfjHB+SjkecflquJdOv
PqaptOyM81t6DFEJTNJC4bfVPV8aUvkr0qW8hIlcMUuO7QvgZREIl7EmKCj6yRwmr2G5o+NsEjD9
gDt9Tgd5F7pEWL4/bZQwIVnDRW/PlSiKGQHpGMazMqsyKP+6ryfOGppisK7/v+KTlrYrlUf7LyFU
y9VmxRDn8dOwdJAg7qXDWsnsGNcrZ6OX8ry3KD48MVPJwf5ueBNDnUdJok22/rmbcxGEG6P8FIJq
/H4L+GXiGTbIeXvtJuptHnZeu9kp+7PXbHl+cpeBMnIwZ90XbjOXYRLOZwNMRl7hquLYB9bTs8KR
moSAQ/arwyo6KwSeYbq1l3LWbkQwgPTh8UE6IUgpaEuy2ybY9OcXrVIACVt9V0WD5iiwp7bv4DYG
lQkhWFGkikiZ+C5wJZ/Uxy+jSMzf4zOYec2toUrAa6wnDI49tMMIFYjkWz6TK+hUi9dqX/bFkBR2
xpL/sLWR2R2/zaoTYaw+JrDrnwFYIlyr6x1iAtu69aoGrLbeUcPFeOrJEMoqPMxYd+b1Pz+4U3Lb
rhoxoPAGW/mMTxb6IUFp0CGmiJJQZID2UDUKllShUgUARfhwDXsI59U4BM4t0Sn/riXlY3g6x5LT
5sV5C11fhvuWdX1YaP0bdPogQmpCcmjk5pNJzBjRirGvjfVo+FFVZd6cCMjghTQkKjTkEJIqwgkf
hl3Wl8F55fI1ZyaXGKCHXxI8hzsJFJIx4ftKkx+Z7GKEvaxl0T7r7uxVuPClNBPyxZPLtOZc0VE3
ISCjvJsKS2lU9q21B2UsJAXOItXlc17PwKP/VSCvI/3oSyeUqXiejIqigos0sk7kRHjuKPWTe2zy
WIoAQqr3dTOxVxWxNKKGfeHRHYu2fA/qxccPgAI8MplJUzUiz8Qd6OPOUeeV4cTGpB0nlfMxaZjr
vKSirfKAO7WbnA/6KqaHXrUxizoJdBht0phAVEuetXlXDMa9Bru2O5fO+JGcMDRuR403xMeSU2CV
ptl1oHaUY4cz8CHfzwocW1ysNkaUud7Xq3FijhNOgTOBHNZDvuPLlk8FC9VY9zS5HLliAouBaY2W
PpsIkUBKh6czLjGO/zV8VIgaxRxNadrntRHmZ1oD5OrXfOn/JL8H/Ogxm7CBjPgOIddidWmD3UqL
QPzKz/unCsBJ4OuuAnkOTlzaloKn93yOilenoS9uppwEbdxEdA5YJ3tGnFCr5o307weS0pga5mVV
xemlmUhRF6nx8OJnxryRsFaLlMNNI1O0RpqfQDSeIywWaelS+y3n8GhO/qzlRjyl54wNSXJuJxNL
UAuvTXN70MxuQG6SEALY47UYtEUwOSitzhvyAjmQSPTZvrXV4/KVgiy14BO8VkTQf1j0zPJVtpzs
VYEzZHS2nY3RimOw6GmrBurH1s9jxoiu4V4Wti5KH9pkJ5Pg/+YVY0mLvTa2BLZkKZ1sz6JJIGn/
pUdG9fvmimXUL02L9gERZV/Ivx52+ZiqTmcHaFeo0CpiMvEeWTtWU1he1ALzgMLTEoK2Sl3IrnuO
j7N4zT5ipA2qyJpwgHfTM7ozAyxogyVYpAlXGC5pzd/N0Rd11TG5Nw0oYdxKODlgiXlaB385K1SB
LzBau9gdN6vo+Tyt3LLwXPyk0gbs9ng7z/3CobetKLSCm5R5OstU36C3pVJLPL5TTkXXHYDpDTMb
jUu64rhIVGsjOxdjZPmuqODYR5yKGepP6Dk+tKWGsf4WXpkBA29MJhggFK4d1lBLJifh3sNLYqjJ
j+kBn1g4Cddq7RgYops/ktrW+mHTmg6KiHgSe5sWePybAU+ybYIan4cXbQcE0e7KiBK9Q3xerxyH
kVqo/0TZy/LTGn7peGKVORAVNbxTYUpm4YnimSdJjuyK31yK8+Ftq45cK4Uz0VFDFmrCfH6u2aHw
cHhE5IyXcINOAyWQhsss4ytjnBLSvnOYAbhlMmm64lQ+As7G+WPycHDpIA9ioSuRVfn5pyxJedmu
0EMinbKj6Tr5ivAd1XkTEbPTNofvbccvt4UOl3uvcOVL/2VUS2hThisZSpr6NdqthyDBDNTrADtX
3Qne3FWci081sVDRpXOy3H/A1EK0a4LCBuGYq7RhG98+vyeMVgUCUNV9EUb/ldmlHwmYd51KIxqP
8IAfVJyVi0ZDSTQrttNZpE+eyzd8HY0hHP18GNxCR+RlQENvnPa4z6Rdxngq5uFEmnD5gdzuBQ+6
ZQixy6VrL7vQGqaOZQsNMoByuqmjU2yGQ0XIQUePDfbgMMStZrt63TM738Ol8SSJ7s6uPRwvufm4
DBYdDfxAaR1jmn74Cjs1p9OM97EuMiVaxvUjHh2yHRaUc5pVoebd/8IBVvoFGXc6JTczXmFpnxRP
+C6a33ajOa6kYJvfZ/q8ZFiD0Z3+LSPox8Vb97GQfsfPWGRBP1grEkRvypgsQkdITGK54zCxHs8v
TYBqJvqrW1+SaYbqbbYm6k7wpS/9geIAd6aQyYSkTZLGkSGPq0bWigwVXokzeBDRW6kNnsAA2kow
od8z7U6DNXWK2NT+bxNa/PLUTXTvz2wEjxUftApcwD/ipBNQ7tS+trixdjK7sGgqR7muqlPm4CI9
fIXjfwtnM8NNFyubvW4g9d2XShh6YhTny9bDNATC5OKXvCDT09DyQfL5fsjfxdJHqVBw1x6VfcBB
Fy6RaZNvyVQZWg15ajgG2svHvLuJKej7acFFuAg781eIhvqJ7O9O1abrxE4+ofXQG53AUvKirlN5
QQdXwqNkKM8n6VOt5t4HNWmwdyosj27c67qjZDsk9JDRU1G4Upi/FYq/WuB5wJiRvh7SeoWH0Z8E
28DMHT7JXuyHo2nDLwUzWNYvI66iPpIdYJvabwf80LkNyKPg7Pl+1djlvOJXQwFDNI3lkLGqWG6f
m6L+Iysh3qEY0zwdPD93JY4b4iVPoCPXIA2nGJX9e28qPGwv1r8H/6J+ihe5MDmWhXVui4C0NBC2
72LLszHu2Dsz/j1B8y/jI4L5B3jLGmqPBagWjtpyl2AuvEt1dtex13Yq1iHMyURBcFJNG+Td93Xr
TGYr7TTClbyZS9X219X5RLwmDsUmrsSSAJUCqXLfZt0bmJd6pePQ3b5DCG44BE5c8eeqIlYM/M3U
TC54kXczIQrA60HXNz4KEdZmbLPKHFnQhSFMoK71U9j0w2xNeLXsdJOuf8DXgp5AJclRBGj0h6jz
72sHGupPa2DL4hS9mRIqxp9P8YX9iHdSvrpEfDqKK3QPO34H13F40UOWYBhHS5GKEC5ccoHeey5a
AMyZW3WLpm6lLPkig1IZjGNeqtJzlHAXxRXpdDUyMaPHzgnBKK+kDriaBA4kK8wEa1QSfnBVOAG2
Du+WFBDcdWXpm4V/4xi3+Tt3bvD1wHZIC3EwtkL/yE7AKyRwrmVNpQS402qkPMRIEN7luvq0banF
oUPG8Io1gp+hWYcIc7pF5MdlOWxwITfU/YPUsCzK/RSOCsFfZ2PHzg4G6uN7sMl74xG5WQqnnIE0
0KYq9fBth8z0YW17hcJ3UMbzO6rkF7g1d+y7tfkpyRrSs6u2mZkuXWvU5gGPbF7D426KEVJjyb2I
y+0I9aKc3bH3kWW4kQI/W4+8zLI7kveo8nhYpTDALVNqlxy31wgYwU2FFRvy92/e+DuO0yu9W8fY
x3La6FQHfpJ9XU26oajPciUNiHwYaOOh4LiveAt9VCK6CpF0BVZdMq8ya1/oBrfjVStBWzAKDIBy
pIfjKvOhIUE2sGl8w8HOVnlK+evE9PIHpjQInSyYKxfUNBiR7kAyNu+lvv+AGAdoavRAmZ0EIiiD
HJ5qZe0OqglMqvbLhUUP14stHNXdZYUIzM5NVOJjeAbHpGBCes2dp9L53UbDvpdTtdLMTJayIWmL
Vr9WFexZlt3bTvO5AFlU/qHPb5XzM+XWd7Sa+mxbvrkLEICC0mVS++kL7maZJad3pt+bA6zn7dWu
QWoV9AXXDhPlUdlxtLcfdTBBYP7wI3pkbzlpo3V/tswPzURkAaMbS+IMk1US/HxMDTvSaC5Xay9j
40pL47ZGqiKp6+kOWfnP6JUr14OIEFIoHBE0ifqGOH33tu6Vi3SD/KNbdyxaLE55FiqZOcgEXrRb
jlbqEfEfn0WDu5l+HYaRTk8o5U1lcCEHuqGmoEZZFwpc/p9QPepWPxY3X55oSgSbLGKWRtAxVqRD
Ot7K0UE/EZfuwrL8nIaG1nwzwFfOZFGeEFmP/lOfS8pv7WfGLtUQjBBWhw/yoFU2kCH5gfkh0AkL
5FXY0sekdG0mHnXUjXbLxnS9HAWBTrRDrkrEL+iL49Qs7JrAA8y1EHaRUjN065kkcXztybV4xFm7
MZBrjE+CU/2WvGVw6gccKQjBuXXZD1TwPfBZYm+As2PDMw4RdDfaflqubfcIdQ6ep65Vy9o0e3Oq
XxKjeOxN3PM2r8JwvTyW63LlWxKEQCQxJzhvj2FYlH3D6dzCfnZSvOwL3G6+peYrY3haxjoi0egU
FgRF93t9yqelgXuvKKZxdBj54i18DtN1rU+Ls5wAfOH7scO0gpzP5UP+x1b8v03f+OQDC6n4A1as
6jMhEX878RpmczJg6U/iG8GGYXV8YHSYwcma/SV0xRTsH3D8t455CXVzIGZO6d9atEAA9OLgP5iB
hoPCSxx04Dxud9yXu8PUysITPD/iCD38fGOWlakuy7WycXOS9gCRE5yx+dKY5C6TwXKZbfpAVrit
tbMd+l/J55dn9v6wthFfPJkPJyVjX1GpQUm9EUEja3cmwG0RDPyIhfB/irxq3/j6EyzeKlLUz84J
pEUUF/FdiLmL8QnuDvy2Cftq68KcYvmj/F6AcI0uopvHDM5Ip0HifHo+hobol0qYmfsPMharddTy
hJJTDHcn1lmxq53GJIUEBAo1Ou4AT1NsUbSzpysUIW5xittj9S2L90IP45DiF7/6mgW73TkCNK5p
S0P1GBYkr07hVJ2PFmrr7yc3SIaTcoGx+2TVzhQcWoYliZ9LauWYxuyGPVfDURzfQNXGKued6khi
/3yicKMhTqrUhApT67QxQ9fUlHj/6Tt/pxTCfMKhte3dF+5qsqVU+P5mm8FzaEeoboUgjPzgXoE6
Z4D+Lt59ZIyzYeWgXkKyTouF5MSJbMB9pICyPsfQ+liH2fjmkeacQOqGHon2lwKu/3bz7T9Jtr3K
iHgV5i++6JfskvCUaa20Lfr3O/2MfZ8JFVpux/1X4tvUVRHkXYzIIIzywMWiWKbnuOZeV26R27cf
0ITE9gYRbfzzgxmPXMiCktxWmFo6X01cTqLCe9j8W1qtugKt3MLh0IAtKbuRFddJJfsuM34+pzOI
xLt+BfsOlIFyp6p9ygxrcbs+VbdSAasxfMHEu6Yn02lcI6szQ4VHkCa9lhU1jldIGAiMOEMrxrsM
Wc/bGvfLZBwBs5oaiC1ELKo41CMQH+zy9bqzs+Y2wMPHQJRRaRKmork1+P8NH8GIRe91/PuAkmU7
bw05lB0t0S5NanyUa+3336tNN6bqz42ERzPENAkOaDfP39/njLRgrHP8j2yH9Ad8hsgibyFmnQ76
JunysaJds3kd1wpeG3VmJbameMAYGn1EX359X7djL9zOdKfkvISZRzFY+xEHlK1wxX4NBSxV3X94
RramQXVD6C3g6p6LSNdOcX33wMffpykzIjjXajnD3IjIIlxJTs1/DAIdk+zR052ktnKcMQ7mH4NH
mq3bFUkVeXmrplnVVJkzIuCO9yloFaMT9REP7RF9Bmm0G8Ir0sUvXQuyxZfRNLLbxHVHgtNMSLRx
Q2nsdlKE2wQRPGzgJ1wMkfmcX747EYb8+tfTTrGrjy4k0xgdBaLJrOiG1GtDF2h6PtyCA75jsJYw
W+oUGCg3D8odOeiUB9WsRf1lVCPYHjUfzzCx4SacU0AAFGekrlCQghTU0W39SCTpFxIF641xZUaX
8y8GZdcT9LuqynvOeqhIXac64uTziNmqNFNeEcmscmmHxiJm2miC9my9DP/2wnuC8rozIdJx1aqR
mqyr5+lFov7iw1fNG5YsmmrjsKzvjeUcAA+0Un+IcdOpAiLoxg44TE1OaWyUDLzrViGveTplqZrH
3jBDjJ2AUfaOfbhjumlnbR2fNNpBQIpoe0wtQL1kZlWRxiC+bZLXMg/5nKCSr/c4dJrF+JUIVtGO
S4lzX+Ag0+vjiOgOS5qq236Nn6L3MYfpwNlNk3bVpZfNLD7klk/A1Pz3I650QcIWxeztg7hGhDUh
NR2MnRsdOTQsRS08vmAGN3sPDA4FsxV3ejdkyv4jqSZR/kPwYq9RmLsatKoXeOMEJqlKJlV8kk8s
ix3B0BwtW7T4Us+RGgVD8eepYJE11Lp6MyO9k6Y0CXL72YQCEgOkH1yy5NkCys6Akm7vOVgJzHAu
GxcCQvN6blRap8UysboCbioPAy3N2nL/NHL3UEYnW/CJ2o3uLUvSoDaA83+WuciIxeDPuohaqzba
MoW9qFnzJCXV9rJUO0Mn7OITDR2KZbuTMibLayZBEWIiyGG5EvDM33z3FgoiW7XqTvoeX+3hn4hE
6bnTiV3kSi+5GPQUoM/pxb85MBmwq7eKfSUonbxf6atRv0U4njDhb7DnbJ2vAoXacGbM/6QqxoDv
6Csv9Gny6jUykvukqF9nPbQP6X8CH3MO26Qt1p2+dX5BmvPW2XRKRkFqwhjK+v6iVy6piBujK5gG
sB87NKNINgIzoMtvZFbtwXOWw85kFQWn5vSzPNCeWHa12tXJzZkYYdNq/oVjE8l86rbG1+vtwXZZ
V6D/9RFwvD8ne+4pp9/Es9Jrm7wqlNsLjAEPvDip6X7F5499o9Dn7oNXu6UvI2PMmOoIYQTU6GU9
paa4gDj90M/ItkuizzbWtAis2pdN7BNY0KOXcd6+FHFDwIEtT2pP5cma9pJsh80znWVFDi/Gu6sB
iN64kOg664X9ebdYYL8RoIWgjYk5zzMROtnzUFuuBWOuWin4C7NURjGGjicdnnCNfecHdEjpmFxU
QlnVamAyMWfTpvumgyagqvy58IdalX+/cl5zWAAzSXVJ6Sm1dDBOelP25d5RtnsnZ2xBqywOK5JF
r2VXUGKBfMO9cE7yYZh2REVBA/heg04qDyRQkD/X7wZO6J82eE+Cf6XL/G7zj4C9cKuG78Qf0zaq
VAyJ9Ig3qNO9esqIsb9xviVsHnZIo3y2gdjmHU4obuFvoajCJlv05UwgW4D9gV01r9zZG7c/BFVn
hNFkWn/+die3+072TXerZLGckR2oMcSCtzg2Fc+3WU8yHWAbjVngPKHv8qqQIVceWFt0UWNmJBfM
h8smMyHkVjbq9tGnuKSj5p4d3lW+0T9XzZVAJ+go99+DWjtRmzAablDOiUSkVFckKWiza2neG9R7
VwnrzvwVsjquLeaSW1RI73w9K0CgVf5ASmtnXu2GcDYnjWdtm1GGTxBldlM1NwXpz5XhqxSCT3IT
xLusm1R4US9PLAzmZQVhS5RP7mjbc1crFgF1+PC5TVq7u6CWXm03dOCOSDcczKrX9tOBxGhRax03
aRsXiKa7KzFhC4WBO2lag6Dfww3G02rJuHsunBu9xz7VqAZJCP+DJVAKVPKN+I5ucqFZU5xRky0x
khLZ6a0I6y7GL7PGmaVTBwzM4xDXuTGyxOpJPS4IgMm7aDuIyWKVZMSkyWCiUe7CXks/BtWUBB/d
Rtq0fKbw2SBE6ou9IYX2My+TeMeK8f2fqR0HzwKHFqCyz7vB3f/UzrGeny9+dFqW+BVmh8Q4UkPO
ItIlibTxvQz6ZSwfnMZ9YDQz6Vt4YNSqV4tmlXZsOIKwEqPvroe3OIaETV7vbxtcdv8yaLej41qI
PtalUmhbkf1iGr5ZYqWtVem6v6my8Hb13GhMe0YwRA3jWEKB/IMy13d0sWnynpAjgmzHu0UyzFzv
rSQymah72IXomGTPW4ifU7Z9ex2nFIm+8IXeQf1M1SMbHMTBW4VsY5Kd01WBYnaljoHNdGZHjmgX
NySPM8QHXgEnwrl5hoi8tyb7G/vRSjvdNIIw/apV2PGfvt9Oie/VeaPVBiYyuxkz8kkAIIszD+d3
fstxXwcSv6AUveby8mS9jq3IbfmciZJoyyVTW2UOzlVS1Sc0bwMPdx+gM1GjR43h9hJuG59xFYxi
VPNDDIr6sAjl0As+1D/9UzYziVVYgkYlevA0UT3VkZQXzK/Vg0xa/m87la4MIgueAFBPP59jAMhl
GVapdPIQGkqICpYsxT6D4fg9IBU+Z8SM/kqCAcJ22Zy+eBLo1lFNNKt8C1u/SQQsG1mKdM6Y7Okt
eQS4qtpgoBQqCSOtud3o+kgiNPOfOJ9qqO5ZtCiP8eSVOVL732m/SQkWtaX27oH7VNR1XmyYBKzO
pQEEP0B78sN+a3o5TlN3sEIGUpfI2UNZ8qFxntAqFXXxIUEYyS7Jq4h4lK2jB886zS/hNqwOwOHz
jhx/54tKZDiE5Ff6rXmQpcHW+91fcSpmM5LxAipL+zrhnE4Mnf6Liaf6Nm0lRN62f7AiHAcXpEPO
BI/Jsy0ajNcqtrEYqLb7LZSXOtVKIq3LBWuKAPL3Qzog62avZa/w9xREetrEqC0CpvDsWHyO5hZ2
ovF346zOSaX71TXpFI6Qn30Jkvn8pFTCt9Be9fL29p4gamS1vDklzBUAB0RtuuvAneTwnEEr6G6K
FeIp6mnGiGC9grxgCxUZxP9GS1xs8vfzGut1xAo0Z2878t4AGyeOEaTEVN7W1+wvzKhxrGwNsbqe
ErCXvsftSx9HQ8CmWSSCosS9NnYnfYtyuDj10oghPK1TzpIjz7vRPOhK7RkUY6hK0nDUMLL1PB5u
zfHQgr56dkCxSHELO6+UKzMgjgQudchj24Gw5SyhjeBTsRGlfe9h0ufsWF8vklboBlZawkwX2KRz
7vxVi3nYRd9SQI/vve/A4lfXMjtuMxCwe+/TBgk7HJ56rrFQDtIfK4RvBZIYfT6cncZ3rnRYEIPh
5fKxF1sKx2bZcTCBvKDDSiWfzeaU8ImKs8yvFYyZ60VJtJX1BCyVicN7wsbhw34C0BurvKtT//MK
FGZOAnnvlrtqjXYqAsA9kIw3cj2a2zXwU2kPaM59QZln9UUvlv4+A50mA437zMSkuxnFUHT+UzM/
BdQguRRJhF75vXpA1HSpXpzy6e4DczCtnzNdRt3/BWUm35bl2xfdWX8BQTHoI3p9K0uc39i71TA9
zgFR+xJwEsT8lEiXZ+fttr+u9d/3NmxAOe+aHNAs3unFYlVxV2TkMM8t/W9fuoSSFJnb3W/Xs/96
NaAUhZ1oXq5CHbn4Zc2Ek129NhOg4V/SftLR9+ZtTpfYbKx+E/T6Hm+lh9uTu5eQ0uGa8f05bhif
s5iUbA3ra5V0ghFqnhfr/6zKZZUofIyKhEsDYXMn1Cyj+FZt3dKQlMHSyaX8iBlyRIOBwVNVGce8
YCdRJQdYYrNNgoos5VQ87KQf5Y/9cKt/US1ATCLElS4dotxTiI7vr206ZKHOmJtBg7zEBVaB+z/x
gZwnSxmfsSG9amPl5/L62Dtx8rxquFGKoARnMTLiXXD2I/oii7es8CZnf7e6suMEJ5Qu8s8BFUdB
mKF7jnIOuhmsAuEPBPa+j5ZBobbYTvF2FiFMRKnfTlKFbllVr+1kP3rTbjBE9o/HhPYgM0eBbs/7
QSBiMcPjSFqrbc2dnOgoeYE5IN8P1iBp6Bjg/RSODRVVZvVMCbt9WmmIKAk+ky0V/VLecTRGbIhS
xOzwOOmlR49x8GwXCyzIsQ3RQOkEBczV8GElIn8ptOO9413r2oH//0xJLfQlY0ujmToaDdUO27qG
gg9eZV3X2Y5uj+8qXzmpjofwwoemOoTx/m25IQZYfUCk48NdGNGZ+5lOG1Kon2nuAz8scqSeZ1lE
Iu+3hZu+IXPaya8WY+7R923avl3KO73aMjmkYpdyG8B9qrxv8N+cmffxiJIL7rmo2dK9MGbB6zcn
OG6YfPcTTb8O3vp9MEwtf+kwxsOpo03+4lf3GUphFep2kPT8EhmlmbeT/6Bb0QDR4/xuwlZNLLl2
r8rqnS9gSLr5sC4RgTD+PH1ElOIeUJ4Q4DjH0ScPr0tYudETl+ZRzX5EiSoPw/yvVD9+Bvjy/cFl
qIKwJgVE9YcujqeptgvmRfAA/BZLWuJu+2leDiBtG57DnSMydh6qF0KUZsaWERLdoqsUFrPUOcdJ
qis59dMnqbuef1BYLkyC2Ri+OFzP6TSeMGPlaFVIkzRmtIqb3IHyPqkj5ogxfjvC+RfPTbarCmxA
jF/5G4lSmAbwgt88SVEE/dV681GyC4Hm/gvRceTSQF/SzPFwkgFLESfPC/a4AoTAojvz0fatkotF
8dlZtpxJuHiunILr7kHIFgkAfg3nkGxEDL3FgBpqDzJ5Jr6j95RaMwUpuxKkGY3qzqx6ZRCQjkqx
eOSy1SxhpZCd2mNc5Ivpe5NZWrhJZP0szNfzrjFbM4TEksh5jabo/zJLMEpwzEazXFl4YQsSUh7I
Ci9QFKATXi2ktdyq9m9e1nrFlBfBfSVTVwXBViOhLiP/e13EqJ0fAP5VBmGxvIJNxcVwDvNNDVAf
GejxgX/NAvgag2DxzImENzXRnq0cN7LdYMqb5QY2/rGkWsGPKAwqjNTwdydrw402Jl/sh5iWNRgJ
VMemM4P1dp1nJ08Mav4uW5kcYRVzB03xu3z+mynPddCxFcoXCQgAWw7u1gWM9fhri8m1Iot8tCNh
gOKOcaP/dsLfm/YmTgo/8mCcZRlKmY7GqAxTBpgaUG12eygTIHhidk2IjaintN0jGUEbuK9ew37/
P79r45p7vPRwur4pIuDQuRJ6oJAPtaTWOPsyVDzmm2AcRdvMmrumdJDesnPUMVyoJV1BU8YYFOX4
PUb+ztPSeMq76U3vsRo+9sTm5RV9/zr5U1jF2cfPFokAhxJKnjDyzeXNxyvA/81Dqxa640+VI2Ju
gwaghcw+q0/cDbhiCg5NX+IgQO8ASOKCBJbsaIrKl2CArFaz0N53D6027kDNvf+wtliUsn8k9UHr
PI6AEpPwpvn8f31BoMipOSfZEI852WRI9IJ6E62h7jgtw3LWie92IkFjSbpYjQ5izBvj7hOGggNr
VCdMYE+XtXyFxQEGk1jgJsr42ktX9Qn7VjPEq99uzxtAEtdHy+L4mVYBOj7xGilg/b87Ll4Bn4h2
8kT8GKh2vBEtoZR/E8O24gg3fYpxQemsh2s666IN6nwr+XZuC+R6UkhMGx9/EEaoeGcYGNChnH9K
fs+bO3LBj5dN8hGIzbR2ZY8q/yRXxXH0ALNTOLkkucGIhxd3DB0KNSkvuRRzxy5B2v1+HjU88BS2
Op5jOZkR+F8iS4jrlhnHMZ31KAumXf1o+sZPQn+LrdNru1B/SBYJ0JNJEeO0NxNz599yGajuAOx+
FvQQyy923so8u0n8AyRaNAhdUbPJOou1iB+VoOcBBM2ASGxaKTUruqROmo9FN8pIa9kmhdyc+7CS
EeB/WHdB8IsPKGQigt/e9qEtJQTS5ZlMOPYKzWEqywufwKQqOswGRr1yK/azhZdOtQK6tAlIbUXT
h0V8lqoNEbVeXPK62MWmYy/2VP2McpUAUHl19rS09VdCRqJ7Z5qnEDiq71VE9189GsWavI0BMKY3
r+hh8C/BC6m7krqn+ILVN2+O87G4qsOCv9uUFD6J5FnhRIa8y71l3JYMg3VafFhDJITBpcUhRSQa
e1ZmKJvpyJ+ay0jtVwtUjXYyse9j2mAN7ivNsOkm7HSjYjV76+pv3c6J1UNHy98JY9wbDarSR+8q
yoqqZbw6JbSqonvlST2VH1jBOcuVHcD+j9D1UfvkbREKkA+30zpAGD4tLMAoH21Qbmy4YIDr+93m
aqjP5e5PComSKToHY//LbZDK+cGT4UNtQn3ttYK46Bpfg4/TfKsWM+t9++MRU2FotrIZgOhmmWwB
tDVd2iZrWLuy6Vsih337Vw6VsoRe94puSWDtYCfYB5lg0H/SC25KZXircDduGMAqduJhHbmnhdCe
fJunWSO8wJK0lti3xSIyxzTQgXVTtdlzvQHL1X6LmdRFrlfZmVY5y2f3y22r09qCdmaah6KigqcA
cx+jQvnQif6TUNGquBHLHB4TOzRJOj75g+hnU8fvqk8kYJY/xsv8/JF3z1MEJF7HOnaFosz/rzSp
d62rkLK228JetvKNrx83AQYcaZ3ctGMlzQDiN0SeFgWSJL7GYQkF7jUYmzTsEWLjU6+Gc3TYD8oU
DYHv0VAjQH8Etx0HIVUI/LYHyj45UekCWAKDQG5yVQ4NUqpJ9CYv/G62WvWAWYO5jbZcmL6YFDe+
a5oLbXtEhKPGsPAQo3LuxeDRGzxGzdeNBaIHavN7V8PxLgNOWvkHTjw5i2g4LRdRJcuTl60qzBdx
Vcj6Gg/ZuGal5onOh9oTiACjQcUVQTfVTWukdYWd7t5eG11JbgMRm2EdPL/LvK7ZaMH/bQQfSaFQ
d7cOktyuDF+rt8IWBnQboo8A8SC/CcWq72QsL1aNHuRg8KYDvoL0zuxwDFScJBw8xuaDA7mYuoQc
UvJT2+FznfYLqzjSMkEZxcnSWlKIszLYCSFqovfJKyjYpzfwMJY6W4mQEeTBWlz4ktkBtYkXtp7x
NUqUyN9L+G1MkNQWpDe+7nKIi/d6RUjVElXwbXqc7B2CbfDNhhRbTxXqA2s4uDWnR9Zo7WcHnV4W
wRwSSQJl7Dn9DOPMXQ3jNpabtv2sHPDXzdx/ZlQJXtdgloE42zRXx3Nv955FKpQL1Es4JqGdsZQz
jJBCJ7mLGMi7mr3QrXqwXrJNGh87Nu+DKPOl2JlDsuBJgHrIbKMVodj2QLy7KuHUaL1+Wullg/hI
I7aNhaAixLkc/bqOZlodKYrFhvPbilWLwCR2oHirFjneNbLgstKjJR/rgmyD0iBep0XE1IwIUua8
UhvwsA4w1ZHcNiPJtuS/tecIo4pZR+TVdU77ugNIO/UqRzKdtqXeLbtLX34AXZZfS0jdU135lRfd
ToCnq0On/0Ih+RWE/NZ1KdjoXm4tLNy2lKDJbkd1x8/FMgHxYHsDsjkvqvwsDPdwvzKaatHGXMVX
fyYm+gFqACs3mX3pzSjRAGzpvJ/6zQ9R1SjX/DExrIzNX6+P3+uX5vg9BC7Er/rVbUhZtoAR1MrB
zYnxtwD8C5SsacNI1bpBTah+8U5vn3unEtMum6Hxa8PqEPRcGAIvAYkCtkLN3fmUDcGcrdeHPkBn
cMXIH/s78NzfFE1PL7qBXRohoStlmJkVjMdgxi0XAGflfgBkjjSNNzXLZR/zVFH9JXGDzj3iBJSV
qn6izdSS8QbssK8exTUyE2QOkvRi7GVGa3vQxiszT4ilrty6DZ51ddCXOLsNZ1ksaCEFYIFaOTgX
ywUK46m44kLZAkkwBf7kMlDR8RrzaW2v4c1pNXSDfDiGEpU4GBJw8XlkbyDSAaEIC75b/kgBLtw7
VFmyZNwGoTTNB4AwitqzNLjo5NLK1nt/fV79aOtQ6OukD3A2Bkb59tlehl5DK9PsQbxTrw2ryZYc
v4RuYyiYfhcMmFn1HXVd2exDQqNqfHSV0P14Z6DTeOiSJya1Xex3Qli446S4s7Wk0oPSXvcDFXft
4N6s+2YPSIZNU7Nl4i7PtBMJL2VVYB8leKlySgGwMir76VaKguI//1eC6N+y812EJhTx1NNTaxhf
XCncO9VkAnYC/Vu7zKESsiTNf5FCtJpsj5HsXgKj/LnfI+0PFfzLdlz7JyaPMAwvgWaYF0aG/EHM
G28LJbOhrBC17BwBdg6txbEmdLWnnpej4j4DHPZXDwN5dnLYAwcIrNBOxrA3m6/UGz1orWtl717U
kz+9uf+DG54LoeWMJ4XGdk3AaCtoTDYFC1P9+1ejlF7nIxxXYs6kPzxrau7LLHeaGMQ4vZfL8O78
I8pc7aYzM1eyFV3Z7sMyk5jyXCmlUxN/KQh3SlOMbUK3uGEm6OPStKm3qRqTvfwla+KPR2RwU+2u
ydy6EUYidX704mzL+50wtt3IW5SbcYb1Vc0vO8pJFAChNSXkKnjNqkU05NR7W0R1dANHjOcr5qEo
UjOnh0/ki74Lo0kPI2jg5XE0RqfJpeSDh1dDT1IAqOpywnMFpjmDyE+w2KCiXzeqhLZH4gye/7p3
yqGnvLKmCP1j7RLcjEEih6rz+5YaKRhtRwHf5aWcIqYWLVLDdnwgJFlZVLmFM+VCOwub2u5G3+9I
r3y5x1VEilMGsGRGVBhWlcxf9c6JZDIsdkhpUARXheicCWyJ8N9xt0lishdK0NhkImklf/Wf3Y6K
DBbh9mv1RzNl+ygXkdufMKJnq3m3ZJcd/iVBGUp2XiaDw3oMVAV0q3lQm94JyRvldy3nt4qw6q9E
MFJVcgSBVTabM3dlL8Kw2Bvzcf+27LhkA2haEKpMh+OjFQ3YG0xv+hiQ1IMFcrvcmOTAcrH3HU2g
ub+bci6W1OC86OaLK29NXk/7FA5MMZmQS4CuajeczQQs+jKka3gqBqAff56UGf8WLy4mV4VvLWPe
36wlvmWFH/ugE5XwLDnq6uYTzYhFivgTyi11MVCTyeazKADrVUUlb0BoGvtOBhvvTRmfZoJBGUC1
bl69ahOSNGtLmzG2eUiev0CthtBh9FNczF67u/6RaP0CVsE/AvHhGKnyBSb5e8Pzg2jr5L5oKNP9
+QMDTSjrJcN1Bxp5/uj9Qhk9jjNRT96QCF8n7YvRASbTH+LjUHGnMJVEPRSICKeypLN3huAN5Fkn
Gip+hiIbrJ2vqlA1xbmAd7DnlP/32fWe5TxsUnny3DWtjxXUnoxQCK/4WfN+KCfE57b4ysRN+9V7
4FZrCcXcL1p/zw6AGWjX8GKMreUvi0/8w/G+QnBPTWnvXqM8TTX0h4KFMSVvZX1MGgzsVp/FmE9J
gW7J6FAk//QM31Fff8nRoWk0KODXHc08xYr7T5PDfT8PUBjuqd/PMNVKJPEm+lHOmzYvwhoxorPA
wQ7mmrCJFcDlYODR5oA6RLzzw0Cui9NOyjw1HOjYC/S0qSLQuPdk9GpooL12PjGc1FKUNYr33Z8l
k3Nu2B8Ttw/453Ax+tW/DlmKTbMctncqG/PPfq8LxbSyvuRsY8PpwYUVI6864L+9FJFV3ztJ8CvF
bVTllDfjSrq+a4EUFqvoGm4KR4IuoguabQUUVSUF6MZkFy3SeJtCaw29jXQDOapwXoaBbKHMCLZM
AgAAXP/80DdTpSLyVBC/c4jCarunGVGhtyqbgWKKOyw+GJ9+lL4g/KQcfxvCpmkI7APCI2l5qTzh
eQvmjzV2Mc7+lKlzZf6f02Jwyg0splkXF2c9Gv2R5/Xneudo0edzqK24uKjkF1gjlzOZ9assUwKO
YwSMntN3A2CwrpysSKwjK4i/0ecUPrJHbeqsY/P1Rn+yvrYwrqFFfY2+TY//z0RQZDjO1DCZ2zpy
g7z8o/HrAwkXtZhxqxQYTecGvRHA0UU8waxoy1uVjv17bvzLTYT1KGphkuFBlt2Uce/H1tbVTT9u
aDGVnNSDX+F+qEOVwUCR+MgGX9M1AyaZ0KlffFeUm+bEgQ2yFkn90+b8+e/9Ua5uSv92AGMSQICZ
kxCIrdjrkFc1xekhFsy7iOQ+aFUytYc+r+MzRVc7qd53nXcdcKVs0dl7UxiA4cKbpAJhdK2yIvxD
El7VYRBEsIdBLvJP/zsZCpmYkNg93fZJu5TWEyXiXrYsdlCeh5X8/WmWHhVmfoKgvWBFeWKdGjow
EZGA4pNQYfJNHehrllV5UhHUIGeFPe9MuPA4+xBSX7MWwyR1Vm/C8tTnSYUUg1dTU+jR5Emq1Hkj
jcD0Y7tf0r5KhvPvsvPEb/GQYkjH2EQet9Jen48wcIk5eAJ7FDkHTAwwyjUwxX+cknqg5FkQw/1B
lW+qNUkTWPrbu0sY5ryatNXnir9tRl5Biq+KfHYPHLWPbzK5baqbiueTfS9Q7/jPE/T05taZf+vK
PTZuQv2uBDp6mFO8/uSb5F0lOuOh/TBcJlTCQ+pCCLcf6Mj6+tcNfRbbvXXa2X4CQzhhTXu0VlhH
JXYHlZaVGI5IoYgGaxSIwfSUKm/o0MBvK8EGAiLPL9TAkDN6Q8akkfGbz0dSqlTiS+3DC55DmBS7
3+egMM+whLdehdFw0lsY+0ZI7D9Q8uQi5YHK8MvxCDqJw69KoJAlXDUXZV3RTUVyKtCzKz9VrTr8
zo0rG/X2ywpTNDat7Ir+dD68EsRnZgwljf4Hfr/Si4VoTW4wKdm15dW/nFLuuvTWreg4Dk3/Bf8B
ytoV02NEthzjCNCF9y+OlgvrJH6exgpsTmm/JWfzIaB1jS8lSPvT5sWZTNE+fj7r+wsNKM+GpXpW
pFV76S+9lLPKNnRZUlJ1MqhH7NhNVxqc/paRiwtHhvWd/mOE2aLQ3SvGuHSJ8FfjNKUBkv48VZ9w
ff/yiftAKpnbEGmlEokW1txa7FdTtC4N/wZ35POnngmEhk2BjAi1xj1WhcePH6eK/7ZbaZT7RigP
UUWOaF5EL5vuML7ogUy2AfwRpBKgLRBXPl/REHIOkv3DkyaHsfs+8XZBbtTsYmNMuv4Epk6OsX1i
n+Nne39sS08VMCwN1AWx6PEQByyBses7dnrn/PQXwiUBYu4G8CJq3wIp7U8jEj4gNViJGprRebdg
fDL6HzXCBTZ3n1prKoGGwVYm8+Y5GYHMJ4s8EPlvXKWkFdz3fMGoPRtYNnp0dM9gXPUV4Sal0kXG
Yj9GHsTK3HOge7jfhIib55SIJLskdLycLothVst4AOmyFIzNR37e/jsrm1idNce1fsMNea21ZsgK
xJv3c3N/OVZ857hAhZGB9PqqtORhiDmQtMX4Iccul1X7IDMVboProgr1VKx6dbwKGptjiIYtlNv9
UAkRlUshDdb6Qwetzt0Dc/F3FKB/VEf8on6AKsi8kMzKVhJ92ZJJE4V5PU+KB1tr2rUvnbsUBkE/
pQ+3N5fk1ZqtBh6MIqH6pfTFXPkmDtzP8YSYx48b273BEMShfj12QZhkQI8q082cgVAaQRFRsEv4
n4YS9NK5r8yFsQxgrQ40fk4Y/kSWEtF75dREarYGk3KoBum5do6CIITXncbrx9mvi7ZCQFKTcE5P
OsUvOAKJGHVRGYAAGq6JfA2PzogdomG4J0d6qUJfWnOv5s4QLjGzHh5pqxF1wgHz+KEftqaBbGGU
5n6SRk74N4X/jh5ozQfj5iSFSPxO8CWEhJMDNqJWy4uRku2iWmf6ntdv3SYLA21R12ls4w/8JWQq
cGMa0wC3tZlDx+1lgu95Wf8+tAbMIMuEEA+47TsyIAquZ1tMTwx6TfabMfr//DXr9Z78eEPavVIx
2D+qDCn3vyTqbX544PfU0jP2BV0HVkPg/ZcCppMZ/QiyMstEIM2tjYiSlwhBt3Gp/sqzJBaODzsx
Oq9mtmZwwEnKMBlvCLj8vE3m07+3889uM4sjYWcIfpjMHZsaXetOxY6ue4luGhyZF6PhwQUf9CbE
f7irOnEJEIgs+l57jxx7NnfTXGB+5O3adSViinNYirxHz/b0J/rmbXhcqZ+jhCGQzbETld2jHcGh
r1KDCOqbWniJIbPWrwxMgRvIoGjlWoeQuYhL//TjPn9SQ7CkIpZZVAIOXilbdhDoLY1zsSukSXz7
touyGJ4FCM1R1vbdiX0eoWjMjyjeRTvkwyHeKnV/waUxoAS6UW/FFPoG8O3KRJaxbFqu2nIqv+Qm
yApC4mTw0wTo19i31FGC+Dd2u+gNRTjfnaRbGSDMPplmM9jvE8onSeUp40hIvAYwkjWwInpMLl6k
AFTPDXqAR7iQiLd5WTIcVir6Xc1TUxWlhN7ekswugxeY/57gIOilrxOHlhL1T8dnWeNxqc3nmzTL
wG6eRn1KbbKYgrjVgiOgWLbKGy0XiN/uyJ5t4YsLfWpqzYfCdNwv5x+dx77H3EZHSAg5467NsC71
aDleM+FhYkwbfisMl7/3jt+ZB/jvytx36tv84bg1lz4wgeNyQYjnCnkvkoVmW1N0p9zG4fLLgg0R
CAJSG93XbY7DzSzLhHnYQQiDJVty/Jtftyd2I4rTxS9H/amtnMwH+hELW++bVBWMnf4FLXQAWxcg
lMFZycxOdwnypln/segn6dQxNjLG2qgyycEt7QCLEUvpcA2CVKvNbFbymlivVEDmaZ2LWGuyeY3u
anPd1n6ZcukkzOCWaYv74jmmsmZrdYJZjdQeXqZiFG4cIG97XNhed2Yi8DrRGWGPbjaFEIb9R4i/
V0ezIK1/N3X7iaxCvTMiA1y2SMRmaFywqUFDPpwzYzBwHRhGGVa9dg9xzherEQHSS8mKg/9v9uFV
3a47PhksHWzcLSd6nspoVVnsMNDFEZFxo/uzwd5SOI+NC8iOhgTvXIAUyBvhuPYVq1K0dFUQ8zPY
rPskm4s6FjFrkBzkhki7uIijvQcrJsu1n6pTah6YVeU8N1aL9jO85GT9cH5xAYZ1kzySMSzsPqPv
bffOx6RCkykobzjAu5QemzKRufyM2UXEo6C4eVt1OEwFJW/+kLEWXM6qeLkjuPtatak2vkZXMsd+
CjtTVJam0agI+rH4fbHtAJk9MmKY/T2m6EX+XWfxYxnrGvhx69Vg1TeRGgTkxxeJkIxGgRG0ma93
+wf2INEL6V3KT/HqPcV2EkqUSOqQ9hJ2ACQ2HBopJcE3TxiP2neWyjAv1XhiOMMG8SxI9YM/e0LV
8aHcEHryjcNTAmVRCzeHEb508k8NCWB0ILxUFMSvX4WaZ1HCATQYJHeMgEuHX4L2Ueg5mPNBJuQI
qpksyIRUtX3PmeOopRpCPPu43RtJN2BxIwKRo1KFa+BxUFFCykGpIXgyoawKDqOh676bniJCEV07
95rx3gY40Y+e7GyOnXLAvZF3ZqaY4ZZF9Fp4Zj8pgO3RDMY02JaGYR40zc1ll9bAwJ7JS5MJBorI
6u1CUxFwjzUlMy/Md+KBs5tc5giicxeUCjlmeFIQB5AcebB5I6jP4xSY3Tag6F4Z4rmjrD25DLJZ
FrkG
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
