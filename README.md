# hitmaker

A collection of code that Geoffrey sent me.

## Getting Started With ModelSim

1. Find the ModelSim instructions that Donovan sent and get it installed.

    a. If you are on Linux, you're on fun mode and will need `fontconfig 2.19.92` and `freetype 2.4.12` in `LD_LIBRARY_PATH`. You can search online for how people do this. Note that everybody else seems to have 32-bit versions of ModelSim, but in my case I had a 64-bit ModelSim so I had to build the 64-bit versions of the libraries.
  
2. Run ModelSim and change into the project directory.
```
ModelSim> cd C:\path\to\hitmaker
ModelSim> pwd
```
You might want to use a batch script to do this like we did in 271, but if you make such a thing, please add it to your .git/info/exclude; please do not commit it to the repo and don't add it to .gitignore. It's specific to your local installation and doesn't belong in repo.

3. Compile everything with `do compile.do`.
```
ModelSim> do compile.do
...
# End time: 21:15:25 on Apr 14,2021, Elapsed time: 0:00:00
# Errors: 0, Warnings: 0
```

4. Set up your simulation with the `vsim` command. Some testbenches have files in the `do/` directory to do all this for you and set it up correctly. 
```
ModelSim> vsim -voptargs="+acc" tb_hit_assembler
```
NOTE: the `-voptargs="+acc"` is important, because without it, ModelSim will optimize away signals that it doesn't think are used (i.e. all of them) and you won't be able to add them to the waveform viewer.

5. Add your waveforms. Just like in 271, you can save these to a `wave.do` file and load them back when you lose them with `do wave.do`. Keep these under `wave/` and please name them after the testbench they correspond to.

6. Run the simulation with `run -all`. If you want to add more waveforms, you can do that and then restart the simulation with `restart -f` before running it again. If you've updated source files, don't forget to recompile them with `do compile.do` before running again. Or, if you're feeling impatient, you can recompile just the one you changed with `vlog the_file_i_changed.sv`.
